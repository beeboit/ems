<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Api')->prefix('employee')->middleware(['jwt.auth'])->group(function() {
    Route::post('me', 'AuthController@me');
    Route::post('leaves', 'EmployeeController@leaves');
    Route::post('projects', 'EmployeeController@projects');
    Route::post('awards', 'EmployeeController@awards');
});
*/
Route::namespace('Api')->prefix('employee')->group(function() {
    Route::post('login', 'AuthController@login');

});
Route::namespace('Api')->prefix('employee')->middleware(['jwt.auth'])->group(function() {
    Route::post('logout', 'AuthController@logout');
    Route::post('change/password', 'AuthController@changePassword');
    Route::post('leaves', 'EmployeeController@leaves');
    Route::post('projects', 'EmployeeController@projects');
    Route::post('me', 'AuthController@me');
    Route::post('awards', 'EmployeeController@awards');
    Route::post('my/attendance', 'EmployeeController@attendance');
    Route::post('my/performance', 'EmployeeController@submitPerformanceForm');
    Route::post('my/feedback/list', 'EmployeeController@myFeedbackList');
    Route::post('team/feedback/list', 'EmployeeController@myTeamFeedbackList');
    Route::post('submit/team/performance', 'EmployeeController@submitEmployeePerformanceForm');
    Route::post('dashboard', 'EmployeeController@dashboard');
    Route::post('leave_by_date', 'EmployeeController@leaveByDate');
});