<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/403', function () {
    return view('403');
});


Auth::routes();

Route::group(['middleware'=>'auth'],function() {

    Route::get('/', 'HomeController@index');
    Route::get('/attendance', 'HomeController@attendance');
    //Route::get('/my/leaves', 'HomeController@myLeaves');
    Route::get('/my/profile', 'EmployeeController@show');
    Route::get('/home', 'HomeController@index')->name('home');
    

    //employee self feedback
    Route::get('/employee_feedback/{id}', 'EmployeePerformanceEvaluationController@index')->name('employee_feedback');
    Route::post('/employee_feedback', 'EmployeePerformanceEvaluationController@submitEmployeeFeedback')->name('self_feedback');

    //submit team evaluation
    Route::post('/employee_evaluation', 'EmployeePerformanceEvaluationController@evaluateEmployeeJobPerformance')->name('evaluate_performance');
    Route::get('/get/designation/{depttId}', 'EmployeeController@getDesignations');
    Route::get('/get/cities/{stateId}', 'EmployeeController@getCities');


    //employee evaluation list
    Route::get('/my/performance/evaluation/list', 'EmployeePerformanceEvaluationController@myPerformanceEvaluationFormList')->name('myPerformanceEvaluationList');
    Route::get('/team/performance/evaluation/list', 'EmployeePerformanceEvaluationController@myTeamPerformanceEvaluationList')->name('myTeamPerformanceEvaluationList');

     Route::get('/change/password', 'EmployeeController@show_change_password')->name('change.password');
     Route::post('/change/password', 'EmployeeController@change_password')->name('update.password');

    Route::group(['middleware'=>'hr'],function() {
        Route::resource('employee', 'EmployeeController', ['names'=>[
            'index' => 'employee.list',
            'create' => 'employee.new',
            'show/{employeeId}' => 'employee.view',
            'store' => 'employee.store',
        ]]);

        Route::resource('leave', 'LeaveController', ['names'=>[
            'index' => 'leave.list',
            'create' => 'leave.new',
            'show/{leaveId}' => 'leave.view',
            'store' => 'leave.store',
        ]]);

        Route::resource('department', 'DepartmentController', ['names'=>[
            'index' => 'department.list',
            'create' => 'department.new',
            'show/{departmentId}' => 'department.view',
            'store' => 'department.store',
        ]]);

        Route::resource('designation', 'DesignationController', ['names'=>[
            'index' => 'designation.list',
            'create' => 'designation.new',
            'show/{designationId}' => 'designation.view',
            'store' => 'designation.store',
        ]]);

        Route::resource('publicholiday', 'PublicHolidayController', ['names'=>[
            'index' => 'publicholiday.list',
            'create' => 'publicholiday.new',
            'show/{publicholidayId}' => 'publicholiday.view',
            'store' => 'publicholiday.store',
        ]]);

        Route::resource('states', 'StatesController', ['names'=>[
            'index' => 'states.list',
            'create' => 'states.new',
            'show/{statesId}' => 'states.view',
            'store' => 'states.store',
        ]]);

        Route::resource('project', 'ProjectController', ['names'=>[
            'index' => 'project.list',
            'create' => 'project.new',
            'show/{projectId}' => 'project.view',
            'store' => 'project.store',
        ]]);


        Route::resource('award', 'AwardController', ['names'=>[
            'index' => 'award.list',
            'create' => 'award.new',
            'show/{awardId}' => 'award.view',
            'store' => 'award.store',
        ]]);
        Route::get('/employee/performance/evaluation/list', 'EmployeePerformanceEvaluationController@emloyeePerformanceEvaluationList')->name('employeePerformanceEvaluationList');
    });


});



Route::get('employee/release/feedback_form', 'EmployeeController@releaseFeedbackForm');



