<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicHoliday extends Model
{
    protected $table = 'public_holidays';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
}
