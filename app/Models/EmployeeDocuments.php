<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDocuments extends Model
{
    protected $table = 'employee_documents';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
}
