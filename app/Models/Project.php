<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
     protected $table = 'projects';
     protected $primaryKey = 'id';
     protected $guarded = ['id'];
     protected $fillable = [
        'project_name', 'start_date', 'end_date', 'deadline_date'
    ];


	public function employees()
	{
	    return $this->belongsToMany('App\User', 'employee_projects', 'fk_projectId', 'fk_employeeId');
	}
}
