<?php

namespace App\Models;


class Validation
{
    public function login($inputs = [])
    {
        $rules = [
            'email' => 'required|string',
            'password' => 'required|min:6'
        ];
        return validator($inputs, $rules);
    }

    public function performance_evaluation($inputs = [])
    {
        $rules = [
            'id'=>'required',
            'my_accomplishments'=>'required',
            'my_weakest_area'=>'required',
            'concerns_with_management'=>'required',
        ];
        return validator($inputs, $rules);
    }

    public function employee_performance_evaluation($inputs = [])
    {
        $rules = [
            'id'=>'required',
            'work_quality'=>'required',
            'dependability'=>'required',
            'job_knowledge'=>'required',
            'communication_skills'=>'required',
            'personality'=>'required',
            'management_ability'=>'required',
            'group_contribution'=>'required',
            'productivity'=>'required',
            'goal_achievement'=>'required',
            'punctuality'=>'required',
            'employee_strongest_point'=>'required',
            'employee_weakest_point'=>'required',
            'employee_improvement_needed'=>'required',
            'additional_beneficial_training'=>'required',
        ];
        return validator($inputs, $rules);
    }

    public function changePassword($inputs = [])
    {
        $rules = [
            'current_password' => 'required|min:6',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:new_password'
        ];
        return validator($inputs, $rules);
    }

    public function leaveDate($inputs = [])
    {
        $rules = [
            'leave_date' => 'required',
        ];
        return validator($inputs, $rules);
    }
}
