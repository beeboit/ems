<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];


    /*public function users()
    {
        return $this->belongsTo('App\User', 'department_id', 'id');
    }*/

    public function department_manager()
    {
        return $this->belongsTo('App\User', 'manager', 'id');
    }
}
