<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeePerformanceEvaluation extends Model
{
    protected $table = 'performance_evaluation';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $fillable = [
      'fk_employeeId', 'reviewing_month', 'reviewing_supervisor', 'my_accomplishments', 'my_weakest_area', 'concerns_with_management', 'work_quality',
       'dependability', 'job_knowledge', 'communication_skills', 'personality', 'management_ability', 'group_contribution', 'productivity', 'goal_achievement',
        'punctuality', 'employee_strongest_point', 'employee_weakest_point', 'employee_improvement_needed', 'additional_beneficial_training', 'hr_comments'
    ];

    public function employee()
    {
        return $this->belongsTo('App\User','fk_employeeId','id');
    }

    public function reviewing_supervisor_details()
    {
        return $this->belongsTo('App\User','reviewing_supervisor','id');
    }

}
