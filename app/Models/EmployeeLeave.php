<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeave extends Model
{
    protected $table = 'employee_leaves';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function employee()
    {
        return $this->belongsTo('App\User','fk_employeeId','id');
    }
}
