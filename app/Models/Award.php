<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $table = 'awards';
     protected $primaryKey = 'id';
     protected $guarded = ['id'];
     protected $fillable = [
        'fk_employeeId', 'award_in_recognition_of', 'month_year', 'certificate'
    ];

    public function employee()
    {
        return $this->belongsTo('App\User','fk_employeeId','id');
    }
}
