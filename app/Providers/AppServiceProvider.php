<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\EmployeePerformanceEvaluation;
use Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*$auth = $this->app['auth'];
        $notifications = [];
        $formReleased = EmployeePerformanceEvaluation::where('fk_employeeId', authId())->whereNull('my_accomplishments')->first();
        if($formReleased != null){
            $notifications[] = ['notification'=>'Performance evaluation form released.', 'url'=>''];
        }
        //dd($notifications);
        $notifications = [];
        View::share('notifications', $notifications);*/
    }
}
