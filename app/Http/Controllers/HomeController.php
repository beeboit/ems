<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeLeave;
use App\Models\PublicHoliday;
Use Carbon\Carbon;
use App\User;
use App\Models\Award;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $today = Carbon::now();
        $lastMonth = Carbon::now()->subMonth()->format('Y-m');
        $data['leaves'] = EmployeeLeave::select('fk_employeeId', 'leave_date', 'leave_type', 'is_paid')->get();
        $data['public_holidays'] = PublicHoliday::select('holiday', 'date')->get();
        $data['upcoming_birthdays'] = User::whereMonth('date_of_birth', $today->month)->select('first_name', 'last_name', 'date_of_birth', 'department_id', 'designation_id')->whereNull('date_of_leaving')->orderBy('date_of_birth')->get();
        $data['employeesOnLeaveToday'] =  EmployeeLeave::where('leave_date', $today->format('Y-m-d'))->get();
        $data['awards'] =  Award::where('month_year','like', $lastMonth.'%')->get();
        return view('dashboard.home',  $data);
    }

    public function attendance($empId=null)
    {
        $leaves = EmployeeLeave::where('fk_employeeId',authId())->select('fk_employeeId', 'leave_date', 'leave_type', 'is_paid')->get();
        $public_holidays = PublicHoliday::select('holiday', 'date')->get();
        $start_day = Carbon::parse('2019-12-01')->format('Y-m-d');
        $today = Carbon::now()->format('Y-m-d');
        return view('dashboard.my_attendance', compact('leaves', 'public_holidays', 'start_day', 'today'));
    }


    public function my_profile(){
        $employeeDetails = Auth::user();
        return view('my_profile', compact('employeeDetails'));
    }
}
