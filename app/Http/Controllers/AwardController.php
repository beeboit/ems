<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Award;
use App\User;
Use Carbon\Carbon;
use Str;
use Session;

class AwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $awardList = Award::orderby('id', 'desc')->get();
        return view('award.list', compact('awardList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $details = [];
        return view('award.details', compact('details'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $rules = [
            'fk_employeeId' => 'required',
            'award_in_recognition_of' => 'required',
            'month_year' => 'required',
            'certificate' =>'mimes:pdf',
        ];
        $this->validate($request, $rules);
        unset($input['_token']);
        if($request->hasFile('certificate')){
            $file = $request->file('certificate');
            $input['certificate'] = Str::random() .'.'. $file->getClientOriginalExtension();
            $file->move('employee_documents/'.$input['fk_employeeId'],$input['certificate']);
        }
        $dt = Carbon::createFromFormat('Y-m', $input['month_year']);
        $input['month_year']= $dt->endOfMonth()->format('Y-m-d');
        Award::updateOrCreate(['id'=>$input['id']], $input);
        Session::flash('flash_message', 'Award details submitted');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = Award::find($id);
        return view('award.details', compact('details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function destroy($id)
    {
         $award = Award::find($id);
         if($award['certificate'] != null){
            unlink('employee_documents/'.$award['fk_employeeId'].'/'.$award['certificate']);
         }
         
        $award->delete();
        Session::flash('flash_message', 'Award deleted');
        return redirect()->back();
    }
}
