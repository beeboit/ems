<?php

namespace App\Http\Controllers\Api;

use App\Models\PublicHoliday;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmployeeLeave;
use Carbon\Carbon;
use Auth;
use App\Models\Validation;
use App\Models\EmployeePerformanceEvaluation;
use App\User;
use App\Models\Award;

class EmployeeController extends Controller
{
    public function leaves()
    {
        try{
            $leaveList = EmployeeLeave::where('fk_employeeId',authId())->select('leave_date', 'leave_type', 'is_paid', 'is_planned', 'leave_reason')->get();
            $response['total_leaves'] = count($leaveList);
            $response['total_paid_leaves'] = count($leaveList->where('is_paid', 1)->count());
            $response['total_unpaid_leaves'] = count($leaveList->where('is_paid', 0)->count());
            $response['total_planned_leaves'] = count($leaveList->where('is_planned', 1)->count());
            $response['total_unplanned_leaves'] = count($leaveList->where('is_planned', 0)->count());
            foreach($leaveList as $leave){
                $leave['leave_date'] = Carbon::parse($leave['leave_date'])->format('d-m-Y');
                $leave['leave_type'] = leave_types($leave['leave_type']);
                $leave['is_paid'] = paid_types($leave['is_paid']);
                if($leave['leave_reason'] == null){
                    $leave['leave_reason'] = '';
                }
                unset($leave['is_planned']);
            }
            $response['leaves'] = $leaveList;
            return apiResponse(true, 200, $response);
        }catch(\Exception $e) {
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function projects()
    {
        try{
            $user = Auth::user();
            $projects = $user->projects;
            foreach($projects as $project){
                if($project['start_date'] != null){
                    $project['start_date'] = Carbon::parse($project['start_date'])->format('d F, Y');
                }if($project['end_date'] != null){
                    $project['end_date'] = Carbon::parse($project['end_date'])->format('d F, Y');
                }if($project['deadline_date'] != null){
                    $project['deadline_date'] = Carbon::parse($project['deadline_date'])->format('d F, Y');
                }
                unset($project['created_at']);
                unset($project['updated_at']);
                unset($project['deleted_at']);
                unset($project['id']);
                unset($project['pivot']);
            }

            //$response['leaves'] = $leaveList;
            return apiResponse(true, 200, $projects);
        }catch(\Exception $e) {
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function awards()
    {
        try{
            $user = Auth::user();
            $awards = $user->awards;
            $awardList = [];
            foreach($awards as $award){
                if($award['month_year'] != null){
                    $award['month_year'] = Carbon::parse($award['month_year'])->format('d F, Y');
                }
                if($award['certificate'] != null){
                    $award['certificate'] = url('employee_documents/'.$award['fk_employeeId'].'/'.$award['certificate']);
                }else{
                    $award['certificate'] = '';
                }

                $awardList[] = ['month'=>$award['month_year'], 'award_title'=>$award['award_in_recognition_of'], 'certificate'=>$award['certificate']];
            }
            return apiResponse(true, 200, $awardList);
        }catch(\Exception $e) {
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function attendance()
    {
        try{
            $attendanceList = [];
            $start = new Carbon('first day of this month');
            $startOfMonth = $start->format('Y-m-d');

            $last = new Carbon('last day of this month');
            $lastOfMonth = $last->format('Y-m-d');

            $date = Carbon::now()->format('Y-m');
            $public_holidays = PublicHoliday::where('date', 'like', $date.'%')->pluck('holiday', 'date')->toArray();
            $leaveList = EmployeeLeave::where('fk_employeeId',authId())->pluck('leave_type', 'leave_date')->toArray();

            for($i=$startOfMonth; $i<=$lastOfMonth; $i++){
                $status = 'Present';
                $color = 'Green';
                $dateOfMonth = Carbon::parse($i);
                if(isset($public_holidays[$i])){
                    $status = $public_holidays[$i];
                    $color = 'Orange';
                }else if(isset($leaveList[$i])){
                    $status = leave_types($leaveList[$i]);
                    $color = 'Red';
                }else if( $dateOfMonth->isSunday()){
                    $status = 'Sunday';
                    $color = 'Green';
                }
                $dateOfMonth = $dateOfMonth->format('d-m-Y');
                $dateOfMonth = Carbon::parse($i)->format('d-m-Y');
                $attendanceList[] = ['date'=>$dateOfMonth, 'status'=>$status, 'color'=>$color];
            }
            return apiResponse(true, 200, $attendanceList);
        }catch(\Exception $e) {
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function submitPerformanceForm(Request $request)
    {
        try{
            \DB::beginTransaction();
            $inputs = $request->input();
            $validation = (new Validation)->performance_evaluation($inputs);
            if($validation->fails()) {
                return apiResponse(false, 406, $validation->getMessageBag());
            }
            $performance = EmployeePerformanceEvaluation::where(['id' => $inputs['id'], 'fk_employeeId' => authId()])->first();
            if($performance == null){
                return apiResponse(false, 404, 'Form not found');
            }
            if($performance['my_accomplishments'] != null){
                return apiResponse(false, 404, 'Form already submitted');
            }

            EmployeePerformanceEvaluation::where(['id' => $inputs['id']])->update(['my_accomplishments'=>$inputs['my_accomplishments'], 'my_weakest_area'=>$inputs['my_weakest_area'], 'concerns_with_management'=>$inputs['concerns_with_management']]);
            \DB::commit();
            return apiResponse(true, 201, 'Form Submitted.');
        }catch(\Exception $e) {
            \DB::rollBack();
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function myFeedbackList(Request $request)
    {
       try{
           $response = EmployeePerformanceEvaluation::where(['fk_employeeId' => authId()])->select('id','fk_employeeId', 'reviewing_month', 'reviewing_supervisor', 'my_accomplishments', 'my_weakest_area', 'concerns_with_management', 'work_quality',
               'dependability', 'job_knowledge', 'communication_skills', 'personality', 'management_ability', 'group_contribution', 'productivity', 'goal_achievement',
               'punctuality')->get();
           foreach($response as $list){
               $status = 'Complete';
               $list['reviewing_month'] = Carbon::parse($list['reviewing_month'])->format('F Y');
               if($list['work_quality'] == null){
                    $list['work_quality'] = $list['dependability'] = $list['job_knowledge'] = $list['communication_skills'] = $list['personality'] = $list['management_ability'] =
                    $list['group_contribution'] = $list['productivity'] = $list['goal_achievement'] = $list['punctuality'] = 0;
                   // $list['employee_strongest_point'] = $list['employee_weakest_point'] = $list['employee_improvement_needed'] =  $list['additional_beneficial_training'] = '';
               }
               if($list['my_accomplishments'] == null){
                   $status = 'Incomplete';
                   $list['my_accomplishments'] = $list['my_weakest_area'] = $list['concerns_with_management'] = '';
               }
               $list['status'] = $status;
               $list['supervisor_name'] = $list->reviewing_supervisor_details->first_name;
               unset($list['reviewing_supervisor_details']);
           }
           return apiResponse(true, 200, $response);
       }catch(\Exception $e) {
           \DB::rollBack();
           return apiResponse(false, 500, __('message.server_error'));
       }
    }

    public function myTeamFeedbackList(Request $request)
    {
        try{
            $performanceList = EmployeePerformanceEvaluation::where(['reviewing_supervisor' => authId()])->select( 'id', 'fk_employeeId', 'reviewing_month', 'reviewing_supervisor', 'my_accomplishments', 'my_weakest_area', 'concerns_with_management', 'work_quality',
                'dependability', 'job_knowledge', 'communication_skills', 'personality', 'management_ability', 'group_contribution', 'productivity', 'goal_achievement',
                'punctuality', 'employee_strongest_point', 'employee_weakest_point', 'employee_improvement_needed', 'additional_beneficial_training', 'hr_comments')->get();
            $response = [];
            foreach($performanceList as $list){
                $status = 'Incomplete';
                $reviewing_month = Carbon::parse($list['reviewing_month'])->format('F Y');
                $employee_name = $list->employee->first_name;
                $supervisor_name = $list->reviewing_supervisor_details->first_name;
                $department = $list->employee->department->department;
                $designation = $list->employee->designation->designation;
                if($list['my_accomplishments'] != null and $list['employee_strongest_point'] != null){
                    $status = 'Complete';
                }
                if($list['work_quality'] == null){
                    $list['work_quality'] = $list['dependability'] = $list['job_knowledge'] = $list['communication_skills'] = $list['personality'] = $list['management_ability'] =
                    $list['group_contribution'] = $list['productivity'] = $list['goal_achievement'] = $list['punctuality'] = 0;
                    $list['employee_strongest_point'] = $list['employee_weakest_point'] = $list['employee_improvement_needed'] =  $list['additional_beneficial_training'] = '';
                }
                if($list['my_accomplishments'] == null){
                    $list['my_accomplishments'] = $list['my_weakest_area'] = $list['concerns_with_management'] = '';
                }
                $response[] = [ 'id'=>$list['id'],
                    'work_quality'=>$list['work_quality'], 'dependability'=> $list['dependability'], 'job_knowledge'=>$list['job_knowledge'],
                    'communication_skills'=> $list['communication_skills'], 'personality'=> $list['personality'], 'management_ability'=>$list['management_ability'],
                    'group_contribution'=>$list['group_contribution'], 'productivity'=>$list['productivity'], 'goal_achievement'=>$list['goal_achievement'],
                    'punctuality'=>$list['punctuality'], 'employee_strongest_point'=>$list['employee_strongest_point'], 'employee_weakest_point'=>$list['employee_weakest_point'],
                    'employee_improvement_needed'=>$list['employee_improvement_needed'], 'additional_beneficial_training'=> $list['additional_beneficial_training'],
                    'my_accomplishments'=>$list['my_accomplishments'], 'my_weakest_area'=>$list['my_weakest_area'], 'concerns_with_management'=>$list['concerns_with_management'],

                   'reviewing_month'=>$reviewing_month, 'employee_name'=>$employee_name, 'reviewing_supervisor'=>$supervisor_name,
                    'department'=>$department, 'designation'=>$designation, 'status'=>$status
                ];
            }
            return apiResponse(true, 200, $response);
        }catch(\Exception $e) {
            \DB::rollBack();
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function dashboard(Request $request)
    {
        try{
            $today = Carbon::now();
            $lastMonth = Carbon::now()->subMonth()->format('Y-m');
            //$today = new Carbon('first day of last month');
            //$today = $today->addDay(1);
            $lastMonth = "2020-01";
            $public_holidays = PublicHoliday::select('holiday', \DB::raw('DATE_FORMAT(date, "%e %M %Y") as date'))->get();
            $data['public_holidays'] = '';
            foreach($public_holidays as $public_holiday){
                $data['public_holidays'] .=  "<p><strong>".$public_holiday['holiday']."</strong>: ".$public_holiday['date']."</p>";
            }

            $upcoming_birthdays = User::whereMonth('date_of_birth', $today->month)->select(\DB::raw('CONCAT("<strong>", first_name," ",last_name, "</strong>") as employee_name'), \DB::raw('DATE_FORMAT(date_of_birth, "%e %M %Y") as date_of_birth'))->whereNull('date_of_leaving')->orderBy('date_of_birth')->get();
            $data['upcoming_birthdays'] = '';
            foreach($upcoming_birthdays as $upcoming_birthday){
                $data['upcoming_birthdays'] .=  "<p><strong>".$upcoming_birthday['employee_name']."</strong>: ".$upcoming_birthday['date_of_birth']."</p>";
            }

            $employeesOnLeaveToday =  EmployeeLeave::select(\DB::raw('CONCAT("<strong>", users.first_name," ",users.last_name, "</strong>") as employee_name'), 'leave_type',
                 \DB::raw('CASE WHEN leave_type = 1 THEN "Full Day" WHEN leave_type = 2 THEN "First Half Leave" else "Second Half Leave" END as leave_type'  ) )
                ->where('leave_date', $today->format('Y-m-d'))
                ->join('users', 'users.id', '=', 'employee_leaves.fk_employeeId')
                ->get();
            $data['employeesOnLeaveToday'] = '';
            foreach($employeesOnLeaveToday as $employeesOnLeave){
                $data['employeesOnLeaveToday'] .=  "<p><strong>".$employeesOnLeave['employee_name']."</strong> -  ".$employeesOnLeave['leave_type']."</p>";
            }

            $award =  Award::select(\DB::raw('CONCAT(users.first_name," ",users.last_name) as employee_name'),'users.picture','award_in_recognition_of', \DB::raw('DATE_FORMAT(month_year, "%M %Y") as Date'))->where('month_year','like', $lastMonth.'%')
                ->join('users', 'users.id', '=', 'awards.fk_employeeId')
                ->first();
            if(isset($award['picture']) and $award['picture'] != null){
                $imgUrl = url('employee_pictures/'.$award['picture']);
            } else{
                $imgUrl = url('dist/img/default_avatar.png');
            }


            $data['awards']['content'] = '<h3 class="profile-username text-center">'.$award['employee_name'].'</h3>

                                      <p class="text-muted text-center">In Recognition of : '.$award['award_in_recognition_of'].' throughout the month of '.$award['Date'].' </p>';
            $data['awards']['picture'] = $imgUrl;
            return apiResponse(true, 200, $data);
        }catch(\Exception $e) { return $e;
            \DB::rollBack();
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function leaveByDate(Request $request)
    {
        $inputs = $request->input();
        $validation = (new Validation)->leaveDate($inputs);
        if($validation->fails()) {
            return apiResponse(false, 406, $validation->getMessageBag());
        }
        try{
            $leaves =  EmployeeLeave::select(\DB::raw('CONCAT("<strong>", users.first_name," ",users.last_name, "</strong>") as employee_name'),'leave_type',
                \DB::raw('CASE WHEN leave_type = 1 THEN "Full Day" WHEN leave_type = 2 THEN "First Half Leave" else "Second Half Leave" END as leave_type'  ) )
                ->where('leave_date', $inputs['leave_date'])
                ->join('users', 'users.id', '=', 'employee_leaves.fk_employeeId')
                ->get();
            $data['leaves'] = '';
            foreach($leaves as $employeesOnLeave){
                $data['leaves'] .=  "<p><strong>".$employeesOnLeave['employee_name']."</strong> -  ".$employeesOnLeave['leave_type']."</p>";
            }
            return apiResponse(true, 200, $data);
        }catch(\Exception $e) {
            \DB::rollBack();
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function submitEmployeePerformanceForm(Request $request)
    {
        try{
            \DB::beginTransaction();
            $inputs = $request->input();
            $validation = (new Validation)->employee_performance_evaluation($inputs);
            if($validation->fails()) {
                return apiResponse(false, 406, $validation->getMessageBag());
            }
            $input = $request->input();

            if(hr_permission() and isset($input['hr_comments'])){
                $input['hr_comments'] = $input['hr_comments'];
            }else{
                $input['hr_comments']= '';
            }

            $checkIfAvailable = EmployeePerformanceEvaluation::where(['id'=>$input['id']])->first();
            if($checkIfAvailable !=null and ($checkIfAvailable['reviewing_supervisor'] == authId() || hr_permission())){
                EmployeePerformanceEvaluation::where(['id'=>$input['id']])->update(array(
                    'work_quality'=>$input['work_quality'], 'dependability'=>$input['dependability'], 'job_knowledge'=>$input['job_knowledge'],
                    'communication_skills'=>$input['communication_skills'], 'personality'=>$input['personality'],  'management_ability'=>$input['management_ability'],
                    'group_contribution'=>$input['group_contribution'], 'productivity'=>$input['productivity'],  'goal_achievement'=>$input['goal_achievement'],
                    'punctuality'=>$input['punctuality'], 'employee_strongest_point'=>$input['employee_strongest_point'], 'employee_weakest_point'=>$input['employee_weakest_point'],
                    'employee_improvement_needed'=>$input['employee_improvement_needed'], 'additional_beneficial_training'=>$input['additional_beneficial_training'],
                    'hr_comments' => $input['hr_comments']
                ));
            }

            \DB::commit();
            return apiResponse(true, 201, 'Form Submitted.');
        }catch(\Exception $e) {
            \DB::rollBack();
            return apiResponse(false, 500, __('message.server_error'));
        }
    }
}
