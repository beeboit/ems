<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Validation;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;

class AuthController extends Controller
{

    /*public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }*/

    public function login(Request $request)
    {
        try {
            $inputs = $request->only('email', 'password');
            $validation = (new Validation)->login($inputs);
            if($validation->fails()) {
                return apiResponse(false, 406, $validation->getMessageBag());
            }

            // authenication code start
            $credentials = [
                'email' => $inputs['email'],
                'password' => $inputs['password']
            ];
            if (! $token =JWTAuth::attempt($credentials)) {
                 return apiResponse(false, 403, 'Invalid Credentials');
            }

            $response = [
                'user_details' => employeeDetails($this->guard()->user()),
               // 'holidays' => public_holidays(),
                'token' => 'bearer '. $token
            ];
            return apiResponse(true, 200, $response);
        }
        catch(\Exception $e) {
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    public function me()
    {
        return apiResponse(true, 200, employeeDetails());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {

        try{
            $token = $request->header('Authorization');
            JWTAuth::invalidate($token);
            return apiResponse(true, 201, 'Successfully logged out');
        }
        catch(\Exception $e) {
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    /** change password **/

    public function changePassword(Request $request)
    {
        $inputs = $request->all();
        $validation = (new Validation)->changePassword($inputs);
        if($validation->fails()) {
            return apiResponse(false, 406, $validation->getMessageBag());
        }

        if (!\Hash::check($inputs['current_password'], auth()->user()->password)) {
            return apiResponse(false, 403, __('message.incorrect_password'));
        }

        try {
            \DB::beginTransaction();
            $update = [
                'password' => \Hash::make($inputs['new_password'])
            ];
            User::where('id', authId())->update($update);
            \DB::commit();
            return apiResponse(true, 201, __('message.password_updated'));
        }
        catch(\Exception $e) {
            \DB::rollBack();
            return apiResponse(false, 500, __('message.server_error'));
        }
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
           // 'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }
}
