<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\EmployeePerformanceEvaluation;
use App\Models\EmployeeDocuments;
use App\Models\EmployeeLeave;
use App\Models\City;
use Illuminate\Support\Facades\Hash;
use Session;
use Carbon\Carbon;
use Str;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leftEmployeeList = User::orderBy('first_name', 'asc')->whereNotNull('date_of_leaving')->get();
        $workingEmployeeList = User::orderBy('first_name', 'asc')->whereNull('date_of_leaving')->get();
        return view('employee.list', compact('leftEmployeeList', 'workingEmployeeList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $details = [];
        $designations = [];
        $today = Carbon::now()->format('Y-m-d');
        return view('employee.details', compact('details', 'designations', 'today'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $file_types = get_file_types();
        $rules = [
            'first_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$input['id']],
            'personal_email' => ['required', 'string', 'email', 'max:255'],
            'department_id' => 'required',
            'designation_id' => 'required',
            'reporting_manager' => 'required',
            'date_of_birth'=>'date|nullable',
            'date_of_joining'=>'required|date',
            'date_of_leaving' => 'date|nullable|after:date_of_joining',
        ];
        if($input['id'] == null){
          /*  $rules = $rules + [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ];*/
            $input['password'] = Hash::make('!Q@W#E$R');
        }

        foreach($file_types as $file_type){
            $rules = $rules + [
                    $file_type => 'mimes:pdf',
            ];
        }
        unset($input['password_confirmation']);
        unset($input['_token']);
        $this->validate($request, $rules);
        unset($input['_token']);
        if(isset($input['is_team_lead'])){
            $input['is_team_lead'] = 1;
        }else{
            $input['is_team_lead'] = 0;
        }

        if($input['date_of_birth'] != null){
            $input['date_of_birth'] = Carbon::parse($input['date_of_birth'])->format('Y-m-d');
        }
        
        if($input['date_of_joining'] != null){
            $input['date_of_joining'] = Carbon::parse($input['date_of_joining'])->format('Y-m-d');
        }

        if($input['date_of_leaving'] != null){
            $input['date_of_leaving'] = Carbon::parse($input['date_of_leaving'])->format('Y-m-d');
        }

        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $input['picture'] = Str::random() .'.'. $file->getClientOriginalExtension();
            $file->move('employee_pictures',$input['picture']);
        }
        $userDetails = User::updateOrCreate(['id'=>$input['id']], $input);

        if($request->file()){
            foreach($request->file() as $key=>$document){
                if(in_array($key, $file_types)){
                    $doc_name = Str::random() .'.'. $document->getClientOriginalExtension();
                    $document->move('employee_documents/'.$userDetails->id, $doc_name);
                    EmployeeDocuments::updateOrCreate(array('fk_employeeId'=>$userDetails->id, 'name'=>$key), array('fk_employeeId'=>$userDetails->id, 'name'=>$key, 'file'=>$doc_name));
                }
            }
        }
        
        Session::flash('flash_message', 'User details submitted');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        if($id == null){
            $id = authId();
        }
        $details = User::find($id);
        $designations = $this->getDesignations($details['department_id']);
        $leaveList = EmployeeLeave::where('fk_employeeId',$id)->orderBy('leave_date', 'desc')->select('fk_employeeId', 'leave_date', 'leave_type', 'is_paid', 'leave_reason')->paginate(20);
        $today = Carbon::now()->format('Y-m-d');
        return view('employee.details', compact('details','designations', 'leaveList', 'today'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDesignations($depttId)
    {
        return designations($depttId);
    }

    public function getCities($stateId)
    {
        $cities = City::where('fk_stateId', $stateId)->pluck('name', 'id')->toArray();
        return $cities;
    }

    public function releaseFeedbackForm()
    {
        $currentDate = Carbon::now()->format('d');

            $date =  new Carbon('last day of this month');
            $month = $date->format('Y-m-d');
            $userList = User::whereNotNull('reporting_manager')->whereNull('date_of_leaving')->get();
            foreach($userList as $user){
                $performanceForm = EmployeePerformanceEvaluation::where('fk_employeeId',$user['id'])->where('reviewing_month', 'like', $month.'%')->first();
                if($performanceForm == null){
                    EmployeePerformanceEvaluation::create(array('fk_employeeId'=>$user['id'], 'reviewing_month'=>$month, 'reviewing_supervisor'=>$user['reporting_manager']));
                }
            }


    }

   public function show_change_password(){
        return view('employee.change_password');
    }

    public function change_password(Request $request){
        $rules = [
           'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
        $this->validate($request, $rules);
        $input = $request->input();
        $input['password'] = Hash::make($input['password']);
        $userDetails = User::where(['id'=>\Auth::user()->id])->update(['password'=>$input['password']]);
        Session::flash('flash_message', 'Password Changed');
        return redirect()->back();
    }
}
