<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\EmployeeProject;
use App\User;
use Session;
use Carbon\Carbon;
class ProjectController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectList = Project::orderby('id', 'desc')->get();
         $today = Carbon::now()->format('Y-m-d');
        return view('project.list', compact('projectList', 'today'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $details = [];
        $assignedToEmployees = [];
        $allEmployees = User::whereNull('date_of_leaving')->orderBy('first_name', 'asc')->pluck('first_name', 'id')->toArray(); 
        return view('project.details', compact('details', 'allEmployees', 'assignedToEmployees'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $rules = [
            'project_name' => 'required',
            'start_date' => 'required|date',
            'deadline_date' => 'date|nullable|after:start_date',
            'end_date' => 'date|nullable|after:start_date',
        ];
        $employeeIds = $input['fk_employeeId'];
        $this->validate($request, $rules);
        unset($input['_token']);

        $projectDetails = Project::updateOrCreate(['id'=>$input['id']], $input);
        EmployeeProject::where('fk_projectId', $projectDetails['id'])->whereNotIn('fk_employeeId', $employeeIds)->delete();
        foreach($employeeIds as $employeeId){
        	EmployeeProject::firstOrCreate(['fk_projectId'=>$projectDetails['id'], 'fk_employeeId'=>$employeeId], ['fk_projectId'=>$projectDetails['id'], 'fk_employeeId'=>$employeeId]);
        }
        
        Session::flash('flash_message', 'Project details submitted');
        return redirect()->back();
    }
    /**
            }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = Project::find($id);
        $assignedToEmployees = $details->employees->pluck('id')->toArray();
        $allEmployees = User::orderBy('first_name', 'asc')->pluck('first_name', 'id')->toArray(); 
        return view('project.details', compact('details', 'allEmployees', 'assignedToEmployees'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->employees()->detach();
        $project->delete();
        Session::flash('flash_message', 'Project deleted');
        return redirect()->back();
    }
}
