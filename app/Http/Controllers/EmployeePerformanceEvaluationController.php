<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Designation;
use App\Models\EmployeePerformanceEvaluation;
use Auth;
use Session;

class EmployeePerformanceEvaluationController extends Controller
{
    public function index($id)
    {
        $details = EmployeePerformanceEvaluation::where(['id'=>$id])->first();
        if($details['fk_employeeId'] == authId() || $details['reviewing_supervisor'] == authId() || hr_permission() == true ||  manager_permission($details['fk_employeeId'] ) == true){
            $ratings = [5=>5,4=>4,3=>3,2=>2,1=>1];
            return view('performance.feedback_details', compact('details', 'ratings'));
        }else{
            return redirect(url('403'));
        }
    }

    public function submitEmployeeFeedback(Request $request)
    {
        $input = $request->input();
        $rules = [
            'my_accomplishments'=>'required',
            'my_weakest_area'=>'required',
            'concerns_with_management'=>'required',
        ];
        $this->validate($request, $rules);
        $checkIfAvailable = EmployeePerformanceEvaluation::where(['fk_employeeId'=>authId(), 'id'=>$input['id']])->first();
        if($checkIfAvailable !=null && $checkIfAvailable['my_accomplishments'] == null){
            EmployeePerformanceEvaluation::where(['fk_employeeId'=>\Auth::user()->id, 'id'=>$input['id']])->update(array('my_accomplishments'=>$input['my_accomplishments'], 'my_weakest_area'=>$input['my_weakest_area'], 'concerns_with_management'=>$input['concerns_with_management']));
        }
        Session::flash('flash_message', 'Feedback submitted');
        return redirect()->back();
    }

    public function evaluateEmployeeJobPerformance(Request $request)
    {
        $rules = [
            'work_quality'=>'required',
            'dependability'=>'required',
            'job_knowledge'=>'required',
            'communication_skills'=>'required',
            'personality'=>'required',
            'management_ability'=>'required',
            'group_contribution'=>'required',
            'productivity'=>'required',
            'goal_achievement'=>'required',
            'punctuality'=>'required',
            'employee_strongest_point'=>'required',
            'employee_weakest_point'=>'required',
            'employee_improvement_needed'=>'required',
            'additional_beneficial_training'=>'required',
        ];
        $this->validate($request, $rules);

        $input = $request->input();
        if(hr_permission() and isset($input['hr_comments'])){
            $input['hr_comments'] = $input['hr_comments'];
        }else{
            $input['hr_comments']= '';
        }
        $checkIfAvailable = EmployeePerformanceEvaluation::where(['id'=>$input['id']])->first();
        if($checkIfAvailable !=null and ($checkIfAvailable['reviewing_supervisor'] == authId() || hr_permission())){
            EmployeePerformanceEvaluation::where(['id'=>$input['id']])->update(array(
                'work_quality'=>$input['work_quality'], 'dependability'=>$input['dependability'], 'job_knowledge'=>$input['job_knowledge'],
                'communication_skills'=>$input['communication_skills'], 'personality'=>$input['personality'],  'management_ability'=>$input['management_ability'],
                'group_contribution'=>$input['group_contribution'], 'productivity'=>$input['productivity'],  'goal_achievement'=>$input['goal_achievement'],
                'punctuality'=>$input['punctuality'], 'employee_strongest_point'=>$input['employee_strongest_point'], 'employee_weakest_point'=>$input['employee_weakest_point'],
                'employee_improvement_needed'=>$input['employee_improvement_needed'], 'additional_beneficial_training'=>$input['additional_beneficial_training'],
                'hr_comments' => $input['hr_comments']
                ));
        }

        Session::flash('flash_message', 'Performance Report submitted');
        return redirect()->back();
	}

    public function myPerformanceEvaluationFormList()
    {
        $feedbackList = EmployeePerformanceEvaluation::where('fk_employeeId', authId())->orderby('id', 'desc')->paginate(20);
        return view('performance.my_feedback_list', compact('feedbackList'));
    }

    public function myTeamPerformanceEvaluationList()
    {
        $feedbackList = EmployeePerformanceEvaluation::where('reviewing_supervisor', authId())->orderby('id', 'desc')->paginate(20);
        return view('performance.feedback_list', compact('feedbackList'));
    }

    public function emloyeePerformanceEvaluationList()
    {
        $feedbackList = EmployeePerformanceEvaluation::orderby('id', 'desc')->paginate(20);
        return view('performance.feedback_list', compact('feedbackList'));
    }
}
