<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\State;
use Session;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statesList = State::orderby('id', 'desc')->get();
        return view('states.list', compact('statesList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $details = [];
        return view('states.details', compact('details'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $rules = [
            'state_name' => 'required',
        ];
        $this->validate($request, $rules);
        unset($input['_token']);

        State::updateOrCreate(['id'=>$input['id']], $input);
        Session::flash('flash_message', 'State submitted');
        return redirect('states');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = State::find($id);
        return view('states.details', compact('details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $states = State::find($id);
        $states->delete();
        Session::flash('flash_message', 'State deleted');
        return redirect()->back();
    }
}
