<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeLeave;
use App\User;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\Input;
use DB;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = Input::get();
        $employeeList = get_employee_list();
        $leaveList = EmployeeLeave::orderBy('leave_date', 'desc');
        if(isset($details['fk_employeeId'])){$leaveList = $leaveList->where('fk_employeeId', $details['fk_employeeId']);}
        if(isset($details['leave_type'])){$leaveList = $leaveList->where('leave_type', $details['leave_type']);}
        if(isset($details['leave_date'])){$leaveList = $leaveList->where('leave_date', Carbon::parse($details['leave_date'])->format('Y-m-d'));}
        $leaveList = $leaveList->paginate(20);
        return view('leaves.list', compact('leaveList', 'details', 'employeeList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $details = [];
        $employeeList = [''=>''] + User::pluck('first_name', 'id')->toArray();
        return view('leaves.add', compact('employeeList', 'details'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $rules = [
            'leave_date' => ['required', 'date'],
            'fk_employeeId' => ['required'],
            'leave_type' => 'required',
            'is_paid' => 'required',
        ];
        $this->validate($request, $rules);
        unset($input['_token']);
        $input['leave_date'] = Carbon::parse($input['leave_date'])->format('Y-m-d');
        if($input['id'] == null){
            EmployeeLeave::updateOrCreate(['leave_date'=>$input['leave_date'], 'fk_employeeId'=>$input['fk_employeeId']], $input);
        }else{
            EmployeeLeave::updateOrCreate(['id'=>$input['id']], $input);
        }
        Session::flash('flash_message', 'Leave details submitted');
        return redirect(url('leave'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = EmployeeLeave::find($id);
        $employeeList = [''=>''] + User::pluck('first_name', 'id')->toArray();
        return view('leaves.add', compact('employeeList', 'details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $leave = EmployeeLeave::find($id);
        $leave->delete();
        Session::flash('flash_message', 'Leave details deleted');
        return redirect()->back();
    }
}
