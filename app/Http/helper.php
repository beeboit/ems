<?php
use App\Models\Department;
use App\Models\Designation;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\EmployeePerformanceEvaluation;
use App\Models\State;

function departments($deptId=null){
    if($deptId == null){
        $departments = [''=>''] + Department::pluck('department', 'id')->toArray();
    }else{
        $departments = Department::where('id', $deptId)->pluck('department')->first();
    }
    return $departments;
}

function designations($deptId, $designationId=null){
    if($designationId == null){
       return $designations = Designation::where('fk_departmentId', $deptId)->pluck('designation', 'id')->toArray();
    }else{
        $designations = Designation::where('fk_departmentId', $deptId)->where('id', $designationId)->pluck('designation')->first();
    }
    return $designations;
}

function team_leads(){
    return [''=>''] + User::where('is_team_lead', 1)->pluck('first_name', 'id')->toArray();
}

function authId(){
    if(isset(\Auth::user()->id)){
        return \Auth::user()->id;
    }
}

function get_notifications(){
    $notifications = [];
    $formReleased = EmployeePerformanceEvaluation::where('fk_employeeId', authId())->whereNull('my_accomplishments')->first();
    if($formReleased != null){
        $notifications[] = ['notification'=>'Performance evaluation form released.', 'url'=>route('employee_feedback', ['id'=>$formReleased['id']])];
    }
    return $notifications;
}

function check_feedback_permission($details){
    if($details['fk_employeeId'] == authId() || $details['reviewing_supervisor'] == authId() || $details->employee->department->manager == authId() || hr() == true){
        return true;
    }else{
        return false;
    }
}

function hr_permission(){
    if(Auth::user()->department_id == 4){
        return true;
    }else{
        return false;
    }
}

function manager_permission($empId){ 
    $user = User::find($empId);
   if(authId() == $user->department->manager){
        return true;
    }else{
        return false;
    }
}
function team_lead_permission(){
    if(authId() == Auth::user()->team_lead){
        return true;
    }else{
        return false;
    }
}

function leave_types($type=null){
    $types = array(''=>'','1'=>'Full Day Leave', '2'=>'First Half Leave', '3'=>'Second Half Leave');
    if($type == null){
        return $types;
    }else{
        return $types[$type];
    }
}


function name_types($type=null){
    $nameTypes = array('1'=>"Father's Name", '2'=>"Husband's Name");
    if($type == null){
        return $nameTypes;
    }else{
        return $nameTypes[$type];
    }
}


function paid_types($type=null){
    if($type == 1){
        return 'Paid';
    }else{
        return 'Unpaid';
    }
}

function get_employee_list(){
    return [''=>''] + User::whereNull('date_of_leaving')->orderBy('first_name', 'asc')->pluck('first_name', 'id')->toArray();
}

function get_all_employees(){
    return [''=>''] + User::orderBy('first_name', 'asc')->pluck('first_name', 'id')->toArray();
}


function gender($id=null){
    $genders = array(''=>'', 1=>'Female', 2=>'Male');
    if($id == null){
        return $genders;
    }else{
        return $genders[$id];
    }
}
/*
function check_performance_evaluation_permission($details){
    if($details){
        return true;
    }else{
        return false;
    }
}*/

function get_states_list(){
    return [''=>''] + State::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
}

function blood_groups($id=null){
    $blood_groups = array(''=>'', 1=>'A+', 2=>'A-', 3=>'B+', 4=>'B-', 5=>'O+', 6=>'O-', 7=>'AB+', 8=>'AB-');
    if($id == null){
        return $blood_groups;
    }else{
        return $blood_groups[$id];
    }
}

function marital_status($id=null){
    $marital_status = array(''=>'', 1=>'Married', 2=>'Un-married', 3=>'Others');
    if($id == null){
        return $marital_status;
    }else{
        return $marital_status[$id];
    }
}

function job_types($id=null){
    $job_types = array(''=>'', 1=>'Part Time', 2=>'Full Time');
    if($id == null){
        return $job_types;
    }else{
        return $job_types[$id];
    }
}

function get_file_types($type=null){
    return $file_types = array('cv', 'adhaar_card', 'pan_card', 'driving_licensce', 'bank_statement_form', 'application_form', 'experience_letter', 'highest_qualification_certificate');
}