<?php
use App\Models\PublicHoliday;
use App\User;

function apiResponse($success, $code, $reply, $extra = [])
{
    $response = [
        'status' => $code,
        'success' => $success,
        'message' => '',
        'errors' => [],
        'result' => [],
        'result_obj' => new ArrayObject(),
        'extra' => $extra ? $extra : new ArrayObject(),
    ];

    if ($code == 200) {
        $response['result_obj'] = $reply;
    } elseif ($code == 404) {
        $response['result'] = $reply;
    } elseif ($code == 406) {
        $response['errors'] = apiErrors($reply);
    } else {
        $response['message'] = $reply;
    }
    return response()->json($response);
}

function apiReply($success, $code, $reply, $extra = [])
{
    $response = [
        'status' => $code,
        'success' => $success,
        'message' => '',
        'errors' => [],
        'result' => [],
        'result_obj' => new ArrayObject(),
        'extra' => $extra ? $extra : new ArrayObject(),
    ];

    $response['message'] = $reply;
    return response()->json($response);
}

function apiErrors($errors) {
    $response = [];
    if (isset($errors) && count($errors) > 0) {
        foreach ($errors->toArray() as $key => $error) {
            $response[] = ['key' => $key, 'value' => $error[0]];
        }
    }
    return $response;
}

function employeeDetails($userDetails = null){
    if($userDetails == null){
        $userDetails = auth()->user();
    }
    $documents = $userDetails->documents->pluck('file', 'name')->toArray();
    $state = $city = $designation = $department = $manager = '';
    if(isset($userDetails['fk_stateId'])){
        $state = $userDetails->state->name;
        unset($userDetails['state']);
    }

    if(isset($userDetails['fk_cityId'])){
        $city = $userDetails->city->name;
        unset($userDetails['city']);
    }

    if(isset($userDetails['reporting_manager'])){
        $manager = $userDetails->reportingManager->first_name;
        unset($userDetails['reportingManager']);
    }

    if(isset($userDetails['department_id']) and $userDetails['department_id'] != null){
        $department = $userDetails->department->department;
        unset($userDetails['department']);
    }

    if(isset($userDetails['designation_id']) and $userDetails['designation_id'] != null and $userDetails['department_id'] != null){
        $designation = $userDetails->designation->designation;
        unset($userDetails['designation']);
    }

    if(isset($userDetails['picture']) and $userDetails['picture'] != null){
        $userDetails['picture'] =  url('employee_pictures/'.$userDetails['picture']);
    }else{
        $userDetails['picture'] =  url('dist/img/default_avatar.png');
    }
    if(isset($userDetails['marital_status']) and  $userDetails['marital_status'] != null){
        $userDetails['marital_status'] = marital_status($userDetails['marital_status']);
    }
    if(isset($userDetails['blood_group']) and  $userDetails['blood_group'] != null){
        $userDetails['blood_group'] = blood_groups($userDetails['blood_group']);
    }
    if(isset($userDetails['gender']) and  $userDetails['gender'] != null){
        $userDetails['gender'] = gender($userDetails['gender']);
    }

    if(isset($userDetails['job_type']) and  $userDetails['job_type'] != null){
        $userDetails['job_type'] = job_types($userDetails['job_type']);
    }

    $userDetails['adhaar_card'] = $userDetails['pan_card'] = $userDetails['driving_license'] = $userDetails['bank_statement_form'] =
    $userDetails['application_form'] = $userDetails['cv'] = $userDetails['experience_letter'] = $userDetails['highest_qualification_certificate'] = '';

    if(isset($documents['adhaar_card'])){
        $userDetails['adhaar_card'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['adhaar_card']);
    }
    if(isset($documents['pan_card'])){
        $userDetails['pan_card'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['pan_card']);
    }
    if(isset($documents['driving_licensce'])){
        $userDetails['driving_license'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['driving_licensce']);
    }
    if(isset($documents['bank_statement_form'])){
        $userDetails['bank_statement_form'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['bank_statement_form']);
    }
    if(isset($documents['application_form'])){
        $userDetails['application_form'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['application_form']);
    }
    if(isset($documents['cv'])){
        $userDetails['cv'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['cv']);
    }
    if(isset($documents['experience_letter'])){
        $userDetails['experience_letter'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['experience_letter']);
    }
    if(isset($documents['highest_qualification_certificate'])){
        $userDetails['highest_qualification_certificate'] = url('employee_documents/'.$userDetails['id'].'/'.$documents['highest_qualification_certificate']);
    }

    if($userDetails['official_phone_number'] == null){
        $userDetails['official_phone_number'] = '';
    }
    if($userDetails['job_location'] == null){
        $userDetails['job_location'] = '';
    }
    if($userDetails['date_of_leaving'] == null){
        $userDetails['date_of_leaving'] = '';
    }
    if($userDetails['address_2'] == null){
        $userDetails['address_2'] = '';
    }
    if($userDetails['bank_address'] == null){
        $userDetails['bank_address'] = '';
    }
    if($userDetails['bank_name'] == null){
        $userDetails['bank_name'] = '';
    }
    if($userDetails['ifsc_code'] == null){
        $userDetails['ifsc_code'] = '';
    }
    if($userDetails['last_name'] == null){
        $userDetails['last_name'] = '';
    }
    if($userDetails['account_holder'] == null){
        $userDetails['account_holder'] = '';
    }
    if($userDetails['account_number'] == null){
        $userDetails['account_number'] = '';
    }
    if($userDetails['pan_card_number'] == null){
        $userDetails['pan_card_number'] = '';
    }
    if($userDetails['adhaar_number'] == null){
        $userDetails['adhaar_number'] = '';
    }

    $userDetails['state'] = $state;
    $userDetails['city'] = $city;
    $userDetails['department'] = $department;
    $userDetails['designation'] = $designation;
    $userDetails['reporting_manager'] = $manager;
    unset($userDetails['fk_cityId']);
    unset($userDetails['fk_stateId']);
    unset($userDetails['department_id']);
    unset($userDetails['designation_id']);
    unset($userDetails['created_at']);
    unset($userDetails['updated_at']);
    //unset($userDetails['reporting_manager']);
    unset($userDetails['documents']);
    unset($userDetails['team_lead']);
    unset($userDetails['is_team_lead']);
    unset($userDetails['comment']);
    return $userDetails;
}

function public_holidays(){
    return PublicHoliday::select('holiday', 'date')->get();
}

function apiStrContains($string, $match) { return strpos($string, $match) !== false; }