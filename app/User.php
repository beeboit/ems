<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_code', 'first_name', 'last_name', 'name_of','name', 'email', 'personal_email',  'phone_number', 'alternate_phone_number', 'blood_group',
        'official_phone_number', 'gender', 'date_of_joining', 'date_of_leaving',
        'date_of_birth', 'address_1', 'address_2', 'fk_stateId', 'fk_cityId', 'pincode', 'marital_status', 'picture', 'password',
        'department_id', 'designation_id', 'reporting_manager', 'job_type', 'job_location', 'bank_address', 'bank_name', 'ifsc_code',
        'account_holder', 'account_number', 'pan_card_number', 'adhaar_number',  'comment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id','id');
    }

    public function designation()
    {
        return $this->belongsTo('App\Models\Designation','designation_id','id');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\EmployeeDocuments', 'fk_employeeId', 'id');
    }

    public function reportingManager()
    {
        return $this->belongsTo('App\User','reporting_manager','id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State','fk_stateId','id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','fk_cityId','id');
    }

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'employee_projects', 'fk_employeeId', 'fk_projectId');
    }

    public function awards()
    {
        return $this->hasMany('App\Models\Award', 'fk_employeeId', 'id');
    }
}
