<?php

return [
    'server_error' => 'Internal server error',
    'incorrect_password' => 'Your password is incorrect',
    'password_updated' => 'Your password updated successfully',

];
