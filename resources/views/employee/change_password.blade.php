@extends('layouts.master')
@section('content')
    <style>
        .details{
            background-color: #e6e1e1;
            padding: 10px;
            height: 40px;
        }
    </style>
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    
    <section class="content-header">
       <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Change Password</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        {{Form::open(array('url'=>route('update.password'), 'method'=>'post'))}}
            <div class="row">
                
                    <!-- general form elements -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Change Password</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="row">
                                       <div class="form-group col-md-12">
                                                <label for="exampleInputFile">Password</label>
                                                {{Form::password('password',  array('class'=>'form-control'))}}
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="exampleInputFile">Confirm Password</label>
                                                {{Form::password('password_confirmation', array('class'=>'form-control'))}}
                                            </div>
                                </div>  
                                <div class="pull-right">
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                </div>                      
                            </div>
                            <!-- /.box-body -->              
                        </div>                        
                    </div>
                 
            </div>
        </form>
    </section>
@endsection
