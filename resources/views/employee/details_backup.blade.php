@extends('layouts.master')
@section('content')
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <section class="content-header">
        <h1>
            Employee
            <small> Details</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Employee Details</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        {{Form::model($details, array('url'=>route('employee.store'), 'method'=>'post', 'files'=> true ))}}
            <div class="row">
                <div class="col-md-3 ">
                    <!-- general form elements -->
                    <div class="col-md-12 no-padding">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Employee Details</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-12"> 
                                        <div class="form-group col-md-12">
                                            @if(isset($details['picture']) and $details['picture'] != null)
                                                @php $imgUrl = url('employee_pictures/'.$details['picture']); @endphp
                                           @else
                                                @php $imgUrl = url('dist/img/default_avatar.png'); @endphp
                                            @endif
                                            <img src="{{$imgUrl}}" width="150" id="previewImage">
                                            <input type="file" name="picture" id="imgInp" accept="image/*">
                                        </div>    
                                        <div class="form-group col-md-12">
                                            <label for="exampleInputEmail1">Gender</label>
                                                <div class="checkbox">
                                                    {!! Form::radio('gender', 1, TRUE) !!} Female
                                                    {!! Form::radio('gender', 2, TRUE) !!} Male
                                                </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                                Mark as Team Lead &nbsp;{!! Form::checkbox('is_team_lead') !!}
                                            </label>
                                        </div>
                                         
                                    </div>
                                </div>                        
                            </div>
                            <!-- /.box-body -->              
                        </div>                        
                    </div>

                    <div class="col-md-12 no-padding">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Documents</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="row">
                                     <div class="form-group col-md-12">
                                        <label>
                                            Documents &nbsp;<input type="file" name="documents[]" multiple>
                                        </label>
                                        @if($details != null and count($details->documents) > 0)
                                            <label>Uploaded Documents</label>
                                            <ul> 
                                                @foreach($details->documents as $document)
                                                    <li>
                                                        <a href="{{url('employee_documents/'.$details['id'].'/'.$document['name'])}}" target="_blank">{{$document['name']}}</a>
                                                    </li>
                                                @endforeach
                                            </ul> 
                                        @endif
                                    </div>
                                </div>                        
                            </div>
                            <!-- /.box-body -->              
                        </div>                        
                    </div> 

                                       
                </div>
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Basic Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        
                        <div class="box-body">
                            <div class="row">
                               
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">First Name</label>
                                            {{Form::text('first_name', null, array('class'=>'form-control'))}}
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Last Name</label>
                                            {{Form::text('last_name', null, array('class'=>'form-control'))}}
                                        </div>                                       
                                        
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputEmail1">Personal Email</label>
                                            {{Form::text('personal_email', null, array('class'=>'form-control'))}}
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Personal Phone Number</label>
                                            {{Form::text('phone_number', null, array('class'=>'form-control'))}}
                                        </div>                        
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Alternate Phone Number</label>
                                            {{Form::text('alternate_phone_number', null, array('class'=>'form-control'))}}
                                        </div>
                                         <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Official Phone Number</label>
                                            {{Form::text('official_phone_number', null, array('class'=>'form-control'))}}
                                        </div>
                                    
                                    </div>
                                     <div class="row">
                                         <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Date of Joining</label>
                                            {{Form::text('date_of_joining', null, array('class'=>'form-control datepicker', 'autocomplete'=>'off'))}}
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Date of Leaving</label> 
                                            {{Form::text('date_of_leaving', null, array('class'=>'form-control datepicker', 'autocomplete'=>'off'))}}
                                        </div>                                                                
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Department</label>
                                            {{Form::select('department_id', departments(), null, array('class'=>'form-control select2', 'id'=>'department', 'data-placeholder'=>'Select Department'))}}
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Designation</label>
                                            {{Form::select('designation_id', $designations, null, array('class'=>'form-control select2', 'id'=>'designation', 'data-placeholder'=>'Select Designation'))}}
                                        </div>
                                        
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Team Lead</label>
                                            {{Form::select('team_lead', team_leads(), null, array('class'=>'form-control select2', 'data-placeholder'=>'Select Team Lead'))}}
                                        </div>
                                         <div class="form-group col-md-6">
                                            <label for="exampleInputFile">Date of Birth</label> 
                                            {{Form::text('date_of_birth', null, array('class'=>'form-control datepicker_old', 'autocomplete'=>'off'))}}
                                        </div>
                                                                
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        <!-- /.box-body -->

                      
                        
                    </div>
                </div>
                 <div class="col-md-3 ">
                    <div class="col-md-12 no-padding">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Login Details</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="row">
                                     <div class="form-group col-md-12">
                                        <label for="exampleInputEmail1">Offical Email</label>
                                        {{Form::text('email', null, array('class'=>'form-control'))}}
                                     </div>
                                     @if($details == null)
                                            <div class="form-group col-md-12">
                                                <label for="exampleInputFile">Password</label>
                                                {{Form::password('password',  array('class'=>'form-control'))}}
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="exampleInputFile">Confirm Password</label>
                                                {{Form::password('password_confirmation', array('class'=>'form-control'))}}
                                            </div>
                                        @endif
                                </div>                        
                            </div>
                            <!-- /.box-body -->              
                        </div>                        
                    </div>    
                    <div class="col-md-12 no-padding">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Bank Details</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="exampleInputEmail1">Account Holder</label>
                                        {{Form::text('account_holder', null, array('class'=>'form-control'))}}
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="exampleInputEmail1">Bank Name</label>
                                        {{Form::text('bank_name', null, array('class'=>'form-control'))}}
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="exampleInputEmail1">IFSC Code</label>
                                        {{Form::text('ifsc_code', null, array('class'=>'form-control'))}}
                                    </div>                                    
                                    <div class="form-group col-md-12">
                                        <label for="exampleInputEmail1">Account Number</label>
                                        {{Form::text('account_number', null, array('class'=>'form-control'))}}
                                    </div>
                                </div>                        
                            </div>
                            <!-- /.box-body -->              
                        </div>                        
                    </div>               
                </div>
                <div class="pull-right">
                    {{Form::hidden('id', null)}}
                     <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                 
            </div>
        </form>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('.select2').select2();
        //fetchDesignation();
        $(document).on("change","#department",{target : $("#designation")},fetchDesignation);
        function fetchDesignation(event)
        {
            var depttId = $(this).val();
            var designationInput = event.data.target;
            var URL = window.location.origin+'/';
            var options = '<option></option>';
            $.ajax({
                url : URL+"get/designation/"+depttId,
                type: 'get',
                success : function(response){
                    var designation = response;
                    $.each(designation,function(id,val){
                        options += "<option value='"+id+"'>"+val+"</option>";
                    })
                },
                complete: function(){
                    designationInput.html(options);
                    designationInput.select2();
                }
            });
        }
        $('.datepicker').datepicker({format:'yyyy-mm-dd'});
        $('.datepicker_old').datepicker({format:'yyyy-mm-dd',   endDate: '+0d'});

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
    </script>
@endsection
