@extends('layouts.master')
@section('content')
    <style>
        .details{
            background-color: #e6e1e1;
            padding: 10px;
            height: 40px;
        }
    </style>
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    
    <section class="content-header">
       <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Employee Details</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
			
        </div>
        {{Form::model($details, array('url'=>route('employee.store'), 'method'=>'post', 'files'=> true ))}}
            <div class="row">
                
                    <!-- general form elements -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Employee Details</h3>
                                @if(hr_permission()== true)
                                    <div class="pull-right box-tools">
                                        <button type="button" class="btn btn-info btn-sm" title="" data-original-title="Edit Details" onclick="showInputs()">
                                            <i class="fa fa-pencil"></i></button>
                                    </div>
                                @endif
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12  no-padding"> 
                                        <div class="col-md-8">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">Employee Code</label>
                                                    {{Form::text('employee_code', null, array('class'=>'form-control'))}}
                                                    <div class="details">@if($details != null){{$details['employee_code']}} @else &nbsp; @endif</div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="checkbox" style="margin-top: 0px !important;  margin-bottom: 0px !important;">
                                                        {!! Form::radio('name_of', 1, TRUE) !!} Father's  Name
                                                        {!! Form::radio('name_of', 2, TRUE) !!} Husband's Name
                                                    </div>
                                                    {{Form::text('name', null, array('class'=>'form-control'))}}
                                                    @if($details != null)  <div class="details" style="background-color: #fff !important; padding:0px; height: 24px;"> <label for="exampleInputEmail1">{{name_types($details['name_of'])}}</label></div>@endif
                                                    <div class="details">@if($details != null){{$details['name']}}@endif</div>
                                                </div>
                                            </div>
                                             <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputEmail1">First Name</label>
                                                    {{Form::text('first_name', null, array('class'=>'form-control'))}}
                                                    <div class="details">@if($details != null){{$details['first_name']}}@endif</div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                     <label for="exampleInputEmail1">Last Name</label>
                                                    {{Form::text('last_name', null, array('class'=>'form-control'))}}
                                                    <div class="details">@if($details != null){{$details['last_name']}}@endif</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="exampleInputFile">Date of Birth</label> 
                                                    {{Form::text('date_of_birth', null, array('class'=>'form-control datepicker_old', 'autocomplete'=>'off'))}}
                                                    <div class="details">@if($details != null){{$details['date_of_birth']}}@endif</div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                     <label for="exampleInputEmail1">Email</label>
                                                     {{Form::text('email', null, array('class'=>'form-control'))}}
                                                    <div class="details">@if($details != null){{$details['email']}}@endif</div>
                                                </div>                                                                                   
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <div class="col-md-12">
                                                 @if(isset($details['picture']) and $details['picture'] != null)
                                                        @php $imgUrl = url('employee_pictures/'.$details['picture']); @endphp
                                                 @else
                                                        @php $imgUrl = url('dist/img/default_avatar.png'); @endphp
                                                 @endif
                                                 <img src="{{$imgUrl}}" width="150" id="previewImage">
                                                 <input type="file" name="picture" id="imgInp" accept="image/*">
                                            </div>
                                            <div class="col-md-12">
                                                <label for="exampleInputEmail1">Gender</label>
                                                    
                                               <div class="checkbox">
                                                    {!! Form::radio('gender', 1, TRUE) !!} Female
                                                    {!! Form::radio('gender', 2, TRUE) !!} Male
                                                </div>
                                                @if($details != null and $details['gender'] != null)
                                                
                                                <div class="details">
                                                    {{gender($details['gender'])}}
                                                </div>
                                                @endif
                                            </div>                                           
                                        </div>
                                    </div>
                                </div>                        
                            </div>
                            <!-- /.box-body -->              
                        </div>                        
                    </div>

                       <div class="col-md-12 ">

                               <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                           <div class="box box-primary nav-tabs-custom">
                                                <ul class="nav nav-tabs">
                                                     <li class="active"><a href="#personal_details" data-toggle="tab">Personal</a></li>
                                                     <li><a href="#official_details" data-toggle="tab">Official</a></li>
                                                     @if($details != null)<li><a href="#leaves" data-toggle="tab">Leaves</a></li>@endif
                                                     <li><a href="#docs" data-toggle="tab">Employee Docs</a></li>
                                                     @if($details != null)
                                                       <li><a href="#projects" data-toggle="tab">Projects</a></li>
                                                       <li><a href="#awards" data-toggle="tab">Awards and Appreciations</a></li>
                                                     @endif
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="personal_details">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                               <div class="col-md-12 form-group">
                                                                  <label for="inputEmail" class="control-label"> Address 1</label>
                                                                     {{Form::text('address_1', null, array('class'=>'form-control'))}}
                                                                     <div class="details">@if($details != null){{$details['address_1']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="inputName" class="control-label">Address 2</label>
                                                                    {{Form::text('address_2', null, array('class'=>'form-control'))}}
                                                                    <div class="details">@if($details != null){{$details['address_2']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-6 form-group">
                                                                   <label for="inputName" class="control-label">State</label>
                                                                   {{Form::select('fk_stateId', get_states_list(), null, array('class'=>'form-control select2', 'data-placeholder'=>'Select State', 'id'=>'state_id'))}}
                                                                    <div class="details">@if(isset($details['fk_stateId'])){{$details->state->name}}@endif</div>
                                                                </div>
                                                                 <div class="col-md-6 form-group">
                                                                   <label for="inputName" class="control-label">City</label>
                                                                   {{Form::select('fk_cityId', [], null, array('class'=>'form-control select2', 'data-placeholder'=>'Select City', 'id'=>'city_id'))}}
                                                                   <div class="details">@if(isset($details['fk_cityId'])){{$details->city->name}}@endif</div>
                                                                 </div>
                                                                <div class="col-md-12 form-group">
                                                                    <label for="inputName" class="control-label">Pincode</label>
                                                                    {{Form::text('pincode', null, array('class'=>'form-control'))}}
                                                                    <div class="details">@if($details != null){{$details['pincode']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Adhaar Card No.</label>
                                                                   {{Form::text('adhaar_number', null, array('class'=>'form-control', 'id'=>'adhaar_number'))}}
                                                                    <div class="details">@if($details != null){{$details['adhaar_number']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Marital Status</label>
                                                                   {{Form::select('marital_status', marital_status(), null, array('class'=>'form-control select2', 'data-placeholder'=>'Marital Status'))}}
                                                                   <div class="details">
                                                                   @if($details != null and $details['marital_status'] != null){{marital_status($details['marital_status'])}}@endif</div>
                                                                </div>
                                                            </div>
                                                             <div class="col-md-6">
                                                                <div class="col-md-12 form-group">
                                                                  <label for="inputEmail" class="control-label">Phone</label>
                                                                     {{Form::text('phone_number', null, array('class'=>'form-control phone'))}}
                                                                    <div class="details">@if($details != null){{$details['phone_number']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-6 form-group">
                                                                   <label for="inputName" class="control-label">Alternate Phone</label>
                                                                   {{Form::text('alternate_phone_number', null, array('class'=>'form-control phone'))}}
                                                                    <div class="details">@if($details != null){{$details['alternate_phone_number']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-6 form-group">
                                                                   <label for="inputName" class="control-label">Official Phone</label>
                                                                   {{Form::text('official_phone_number', null, array('class'=>'form-control phone'))}}
                                                                    <div class="details">@if($details != null){{$details['official_phone_number']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Personal Email</label>
                                                                   {{Form::email('personal_email', null, array('class'=>'form-control'))}}
                                                                    <div class="details">@if($details != null){{$details['personal_email']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Blood Group</label>
                                                                   {{Form::select('blood_group', blood_groups(), null, array('class'=>'form-control select2', 'data-placeholder'=>'Blood Group'))}}
                                                                    <div class="details">@if(isset($details['blood_group'])) {{blood_groups($details['blood_group'])}}@endif</div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Pan Card No.</label>
                                                                   {{Form::text('pan_card_number', null, array('class'=>'form-control'))}}
                                                                    <div class="details">@if($details != null){{$details['pan_card_number']}}@endif</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Comment</label>
                                                                   {{Form::text('comment', null, array('class'=>'form-control'))}}
                                                                    <div class="details">@if($details != null){{$details['comment']}}@endif</div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane " id="official_details">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                               <div class="form-group col-md-12">
                                                                    <label for="exampleInputFile">Date of Joining</label>
                                                                    {{Form::text('date_of_joining', null, array('class'=>'form-control datepicker', 'autocomplete'=>'off'))}}
                                                                   <div class="details">@if($details != null){{$details['date_of_joining']}}@endif</div>
                                                               </div>

                                                                <div class="form-group col-md-12">
                                                                   <label for="exampleInputFile" class="control-label">Job Type</label>
                                                                   {{Form::select('job_type', job_types(), null, array('class'=>'form-control select2', 'data-placeholder'=>'Select Job Type'))}}
                                                                    <div class="details">@if(isset($details['job_type'])) {{job_types($details['job_type'])}}@endif</div>
                                                                </div>

                                                                <div class="form-group col-md-12">
                                                                    <div class="form-group col-md-6 no-padding">
                                                                        <label for="exampleInputFile">Department</label>
                                                                        {{Form::select('department_id', departments(), null, array('class'=>'form-control select2', 'id'=>'department', 'data-placeholder'=>'Select Department'))}}
                                                                        <div class="details">@if(isset($details['department_id'])){{$details->department->department}}@endif</div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="exampleInputFile">Designation</label>
                                                                        {{Form::select('designation_id', $designations, null, array('class'=>'form-control select2', 'id'=>'designation', 'data-placeholder'=>'Select Designation'))}}
                                                                        <div class="details">@if(isset($details['designation_id'])){{$details->designation->designation}}@endif</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             <div class="col-md-6">
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputFile">Date of Leaving</label>
                                                                    {{Form::text('date_of_leaving', null, array('class'=>'form-control datepicker', 'autocomplete'=>'off'))}}
                                                                    <div class="details">@if($details != null){{$details['date_of_leaving']}}@endif</div>
                                                                </div>
                                                                <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Reporting Manager</label>
                                                                   {{Form::select('reporting_manager', get_employee_list(), null, array('class'=>'form-control select2',  'data-placeholder'=>'Select Reporting Manager'))}}
                                                                    <div class="details">@if(isset($details['reporting_manager'])){{ ($details->reportingManager->first_name) }}@endif</div>
                                                                </div>

                                                                <div class="col-md-12 form-group">
                                                                   <label for="inputName" class="control-label">Job Location</label>
                                                                   {{Form::text('job_location', null, array('class'=>'form-control', 'placeholder'=>'Job Location'))}}
                                                                    <div class="details">@if($details != null){{$details['job_location']}}@endif</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h4>Employee Salary Account Details</h4>
                                                                <div class="form-group col-md-6">
                                                                    <div class="form-group col-md-12">
                                                                        <label for="exampleInputEmail1">Bank Name</label>
                                                                        {{Form::text('bank_name', null, array('class'=>'form-control'))}}
                                                                        <div class="details">@if($details != null){{$details['bank_name']}}@endif</div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                         <label for="exampleInputEmail1">Account Number</label>
                                                                        {{Form::text('account_number', null, array('class'=>'form-control'))}}
                                                                        <div class="details">@if($details != null){{$details['account_number']}}@endif</div>
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label for="exampleInputEmail1">IFSC Code</label>
                                                                        {{Form::text('ifsc_code', null, array('class'=>'form-control'))}}
                                                                        <div class="details">@if($details != null){{$details['ifsc_code']}}@endif</div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <div class="form-group col-md-12">
                                                                        <label for="exampleInputEmail1">Account Holder</label>
                                                                        {{Form::text('account_holder', null, array('class'=>'form-control'))}}
                                                                        <div class="details">@if($details != null){{$details['account_holder']}}@endif</div>
                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <label for="exampleInputEmail1">Bank Address</label>
                                                                        {{Form::text('bank_address', null, array('class'=>'form-control'))}}
                                                                        <div class="details">@if($details != null){{$details['bank_address']}}@endif</div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if($details != null)
                                                    <div class="tab-pane" id="leaves">
                                                        <div class="row">
                                                            <div class="col-md-12 ">
                                                                <div class="col-md-4"><strong>Total Leaves : </strong>{{count($leaveList)}}</div>
                                                                <div class="col-md-2"><strong> Paid Leaves : </strong>{{$leaveList->where('is_paid', 1)->count()}}</div>
                                                                <div class="col-md-2"><strong> Unpaid Leaves : </strong>{{$leaveList->where('is_paid', 0)->count()}}</div>
                                                                <div class="col-md-2"><strong> Planned Leaves : </strong>{{$leaveList->where('is_planned', 1)->count()}}</div>
                                                                <div class="col-md-2"><strong> Unplanned Leaves : </strong>{{$leaveList->where('is_planned', 0)->count()}}</div>
                                                            </div>
                                                        </div>
                                                        <table id="data" class="table table-bordered table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Leave Date</th>
                                                                <th>Leave Type</th>
                                                                <th>Leave Reason</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($leaveList as $leave)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>{{\Carbon\Carbon::parse($leave['leave_date'])->format('d-M-Y')}}</td>
                                                                    <td>{{leave_types($leave['leave_type'])}}</td>
                                                                    <td>{{$leave['leave_reason']}}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>

                                                        </table>
                                                        <div class="pagination pull-right">{{$leaveList->links()}}</div>
                                                    </div>
                                                    @endif
                                                    <div class="tab-pane " id="docs">
                                                        <div class="row">
                                                            <div class="form-group col-md-12"> <h4>Documents</h4></div>
                                                            @php $documents = []; @endphp
                                                            @if($details != null) @php $documents = $details->documents->pluck('file', 'name')->toArray(); @endphp @endif
                                                            
                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">Adhaar Card</label>
                                                               <div class="col-md-9">
                                                                     @if(isset($documents['adhaar_card'])) 
                                                                       <a  href="{{url('employee_documents/'.$details['id'].'/'.$documents['adhaar_card'])}}" target="_blank">
                                                                        <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                       <span class="btn-xs btn-primary">View Adhaar Card</span></a><br>
                                                                    @endif
                                                                    <input type="file" name="adhaar_card"  accept="application/pdf">
                                                               </div>

                                                               
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">Pan Card</label>
                                                                <div class="col-md-9">
                                                                    @if(isset($documents['pan_card'])) 
                                                                   <a href="{{url('employee_documents/'.$details['id'].'/'.$documents['pan_card'])}}" target="_blank">
                                                                   <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                   <span class="btn-xs btn-primary">View Pan Card</span>
                                                                   </a><br> @endif
                                                                   <input type="file" name="pan_card"  accept="application/pdf">
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">Driving Licence</label>
                                                                <div class="col-md-9">
                                                                    @if(isset($documents['driving_licensce'])) 
                                                                   <a href="{{url('employee_documents/'.$details['id'].'/'.$documents['driving_licensce'])}}" target="_blank">
                                                                    <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                   <span class="btn-xs btn-primary">View driving_licensce</span>
                                                                   </a><br> @endif
                                                                   <input type="file" name="driving_licensce"  accept="application/pdf">
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">Bank Detail Form</label>
                                                                <div class="col-md-9">
                                                                    @if(isset($documents['bank_statement_form'])) 
                                                                   <a href="{{url('employee_documents/'.$details['id'].'/'.$documents['bank_statement_form'])}}" target="_blank">
                                                                    <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                   <span class="btn-xs btn-primary">View bank_statement_form</span></a><br> @endif
                                                                   <input type="file" name="bank_statement_form" accept="application/pdf">
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">Application Form</label>
                                                                <div class="col-md-9">
                                                                    @if(isset($documents['application_form'])) 
                                                                   <a href="{{url('employee_documents/'.$details['id'].'/'.$documents['application_form'])}}" target="_blank">
                                                                   <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                   <span class="btn-xs btn-primary">View application_form</span></a><br> @endif
                                                                   <input type="file" name="application_form" accept="application/pdf">
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">CV</label>
                                                                <div class="col-md-9">
                                                                   @if(isset($documents['cv'])) 
                                                                   <a  href="{{url('employee_documents/'.$details['id'].'/'.$documents['cv'])}}" target="_blank">
                                                                   <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                   <span class="btn-xs btn-primary">View CV</span></a><br> @endif
                                                                   <input type="file" name="cv" accept="application/pdf">
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">Last Experience Certificate</label>
                                                                <div class="col-md-9 ">
                                                                    @if(isset($documents['experience_letter'])) 
                                                                   <a class="text-center" href="{{url('employee_documents/'.$details['id'].'/'.$documents['experience_letter'])}}" target="_blank">
                                                                   <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                   <span class="btn-xs btn-primary">View Experience Letter</span><br></a> @endif
                                                                   <input type="file" name="experience_letter" accept="application/pdf">
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                               <label for="exampleInputFile" class="col-md-3">Highest Qualification Certificate</label>
                                                                <div class="col-md-9">
                                                                    @if(isset($documents['highest_qualification_certificate'])) 
                                                                   <a href="{{url('employee_documents/'.$details['id'].'/'.$documents['highest_qualification_certificate'])}}" target="_blank">
                                                                   <img src="{{url('dist/img/pdf.png')}}"><br>
                                                                   <span class="btn-xs btn-primary"> View Highest Qualification Certificate</span><br></a> @endif
                                                                   <input type="file" name="highest_qualification_certificate" accept="application/pdf">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  <!-- /.tab-pane -->
                                                @if($details != null)
                                                    <div class="tab-pane" id="projects">
                                                      
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Project</th>
                                                                <th>Start Date</th>
                                                                <th>End Date</th>
                                                                <th>Deadline </th>
                                                                <th>Deadline </th>
                                                             </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($details->projects as $project)
                                                                    <tr @if($project['end_date'] != null and $project['end_date'] <= $today) style="background-color: green; color:#fff;" @endif>
                                                                        <td>{{ $loop->iteration }}</td>
                                                                        <td>{{$project['project_name']}}</td>
                                                                        <td>@if($project['start_date'] != null){{\Carbon\Carbon::parse($project['start_date'])->format('d F, Y')}} @endif</td>
                                                                        <td>@if($project['end_date'] != null) {{\Carbon\Carbon::parse($project['end_date'])->format('d F, Y')}} @endif</td>
                                                                        <td>@if($project['deadline_date'] != null) {{\Carbon\Carbon::parse($project['deadline_date'])->format('d F, Y')}} @endif</td>
                                                                        <td>
                                                                            @if($project['end_date'] != null and $project['end_date'] <= $today) 
                                                                                @for($i=1; $i<=5;$i++)
                                                                                    <i class="fa fa-2x fa-star star"></i>
                                                                                @endfor
                                                                            @else 
                                                                                In Progress
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>

                                                        </table>
                                                        <div class="pagination pull-right"></div>
                                                    </div>
                                                    <div class="tab-pane" id="awards">
                                                      
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Award In recognition of</th>
                                                                <th>Month</th>
                                                                <th>View Certificate</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                               @foreach($details->awards as $award)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$award['award_in_recognition_of']}}</td>
                                                                        <td>{{\Carbon\Carbon::parse($award['month_year'])->format(' F, Y')}}</td>
                                                                        <td>  
                                                                            @if($award['certificate'] != null)
                                                                                <a  href="{{url('employee_documents/'.$award['fk_employeeId'].'/'.$award['certificate'])}}" target="_blank">
                                                                                    <img src="{{url('dist/img/pdf.png')}}" height=50><br>
                                                                                       <span class="btn-xs btn-primary">View Certificate</span>
                                                                                </a>
                                                                            @else
                                                                                 <span>Not available</span>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>

                                                        </table>
                                                        <div class="pagination pull-right"></div>
                                                    </div>
                                                @endif
                                                </div>
                                            </div>

                                    </div>
                                </div>
                                <!-- /.box-body -->

                        </div> 
                @if(hr_permission()== true)
                    <div class="pull-right">
                        {{Form::hidden('id', null)}}
                         <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                @endif
                 
            </div>
        </form>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('#adhaar_number').on('keypress change', function () {
            $(this).val(function (index, value) {
                return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            });
        });

        $('.phone').on('keypress change', function () {
            $(this).val(function (index, value) {
                return value.replace(/\W/gi, '').replace(/(.{5})/g, '$1 ');
            });
        });
        var URL = window.location.origin+'/';
            
        $('.select2').select2();
        //fetchDesignation();
        $(document).on("change","#department",{target : $("#designation")},fetchDesignation);
        function fetchDesignation(event)
        {
            var depttId = $(this).val();
            var designationInput = event.data.target;
            var options = '<option></option>';
            $.ajax({
                url : URL+"get/designation/"+depttId,
                type: 'get',
                success : function(response){
                    var designation = response;
                    $.each(designation,function(id,val){
                        options += "<option value='"+id+"'>"+val+"</option>";
                    })
                },
                complete: function(){
                    designationInput.html(options);
                    designationInput.select2();
                }
            });
        }

        $(document).on("change","#state_id",{target : $("#city_id")},fetchCities);
        function fetchCities(event)
        {
            var stateId = $(this).val();
            var cityInput = event.data.target;
            var URL = window.location.origin+'/';
            var options = '<option></option>';
            $.ajax({
                url : URL+"get/cities/"+stateId,
                type: 'get',
                success : function(response){
                    var city = response;
                    $.each(city,function(id,val){
                        options += "<option value='"+id+"'>"+val+"</option>";
                    })
                },
                complete: function(){
                    cityInput.html(options);
                    cityInput.select2();
                }
            });
        }


        $('.datepicker').datepicker({format:'yyyy-mm-dd', autoclose: true});
        $('.datepicker_old').datepicker({format:'yyyy-mm-dd',   endDate: '+0d', autoclose: true});

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });

    @if(hr_permission() == false)
            $('input').remove();
            $('.select2').remove();
            $('.checkbox').remove();
    @else
        @if($details == null && hr_permission()== true)
              $('.details').hide();
		@else
			@if(count($errors) > 0 && hr_permission()== true)
              $('.details').hide();
			@else
				@if($details != null && hr_permission()== true)
					$('input').hide();
					$('.select2').hide();
					$('.checkbox').hide();
					function showInputs(){
						$('input').toggle();
						$('.select2').toggle();
						$('.details').toggle();
						$('.checkbox').toggle();
					}
				@endif
			@endif
			
        @endif
    @endif
</script>
@endsection
