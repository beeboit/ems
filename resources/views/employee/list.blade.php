@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Employee List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#working_employees" data-toggle="tab">Current Working Employees</a></li>
                        <li><a href="#left_employees" data-toggle="tab">Left Employees</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="working_employees">
                             <h3 class="text-center">Working Employee List</h3>
                           
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <table id="data" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <th>Department</th>
                                            <th>Designation</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($workingEmployeeList as $employee)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$employee['first_name'] . ' '. $employee['last_name'] }}</td>
                                            <td>{{$employee['email']}}</td>
                                            <td>{{$employee['phone_number']}}</td>
                                            <td>@if(isset($employee->department->department)) {{$employee->department->department}} @endif</td>
                                            <td>@if(isset($employee->designation->designation)) {{$employee->designation->designation}} @endif</td>
                                            <td>
                                                <a href="{{route('employee.show', ['id'=>$employee['id']])}}" class="btn-sm btn-primary"> View Details</a>
                                               {{-- <a href="{{url('leave?fk_employeeId='.$employee['id'])}}" class="btn-sm btn-primary"> View Leaves</a>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="tab-pane " id="left_employees">
                                 <h3 class="text-center">Left Employee List</h3>
                           
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <table id="data1" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <th>Department</th>
                                            <th>Designation</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($leftEmployeeList as $employee)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$employee['first_name'] . ' '. $employee['last_name'] }}</td>
                                            <td>{{$employee['email']}}</td>
                                            <td>{{$employee['phone_number']}}</td>
                                            <td>@if(isset($employee->department->department)) {{$employee->department->department}} @endif</td>
                                            <td>@if(isset($employee->designation->designation)) {{$employee->designation->designation}} @endif</td>
                                            <td>
                                                <a href="{{route('employee.show', ['id'=>$employee['id']])}}" class="btn-sm btn-primary"> View Details</a>
                                                <a href="{{url('leave?fk_employeeId='.$employee['id'])}}" class="btn-sm btn-primary"> View Leaves</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
          //  $('#data1').DataTable()
            $('#data, #data1').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
