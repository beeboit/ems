@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <section class="content-header">
       <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Department List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Department List</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table id="data" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Department</th>
                                <th>Manager</th>
                                <th>View Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($departmentList as $department)
                                <tr>
                                    <td>{{$department['id']}}</td>
                                    <td>{{$department['department']}}</td>
                                    <td>@if(isset($department->department_manager->first_name)){{$department->department_manager->first_name}}@endif</td>
                                    <td><a href="{{route('department.show', ['id'=>$department['id']])}}" class="btn btn-primary"> View Details</a></td>
                                   {{-- <td>
                                        {{Form::open(array('url'=>route('department.destroy', ['department'=>$department]), 'method'=>'delete'))}}
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        {{Form::close()}}
                                       
                                    </td> --}}
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->


                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#data').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
