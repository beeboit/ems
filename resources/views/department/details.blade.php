@extends('layouts.master')
@section('content')
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Department Details</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Department Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {{Form::model($details, array('url'=>route('department.store')))}}
                    <div class="box-body">
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Department</label>
                            {{Form::text('department',  null, array('class'=>'form-control'))}}
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-4">
                                <label for="exampleInputFile">Manager</label>
                                {{Form::select('manager', get_employee_list(), null, array('class'=>'form-control select2', 'id'=>'department', 'data-placeholder'=>'Select Manager'))}}
                            </div>
                        </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::hidden('id', null)}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script>
        $('.select2').select2();
        //fetchDesignation();
        $(document).on("change","#department",{target : $("#designation")},fetchDesignation);
        function fetchDesignation(event)
        {
            var depttId = $(this).val();
            var designationInput = event.data.target;
            var URL = window.location.origin+'/';
            var options = '<option></option>';
            $.ajax({
                url : URL+"get/designation/"+depttId,
                type: 'get',
                success : function(response){
                    var designation = response;
                    $.each(designation,function(id,val){
                        options += "<option value='"+id+"'>"+val+"</option>";
                    })
                },
                complete: function(){
                    designationInput.html(options);
                    designationInput.select2();
                }
            });
        }
    </script>
@endsection
