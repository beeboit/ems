@extends('layouts.master')
@section('content')
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <section class="content-header">
        <h1>
            Leave
            <small> Details</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Leave Details</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leave Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {{Form::model($details, array('url'=>route('leave.store')))}}
                    <div class="box-body">
                        <div class="form-group  col-md-3">
                            <label for="exampleInputEmail1">Select Employee</label>
                            {{Form::select('fk_employeeId', $employeeList, null, array('class'=>'form-control select2', 'data-placeholder'=>'Select Employee'))}}
                        </div>

                        <div class="form-group col-md-3">
                            <label for="exampleInputEmail1">Leave Types</label>
                            {{Form::select('leave_type', leave_types(), null, array('class'=>'form-control select2', 'data-placeholder'=>'Select Leave Type'))}}
                        </div>

                        <div class="form-group col-md-2">
                            <label>Leave Date:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {{Form::text('leave_date', null, array('class'=>'form-control pull-right datepicker_new', 'autocomplete'=>'off'))}}
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Paid</label>
                            <div class="checkbox">
                                {!! Form::radio('is_paid', 1, TRUE) !!} Yes
                                {!! Form::radio('is_paid', 0, TRUE) !!} No
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Planned</label>
                            <div class="checkbox">
                                {!! Form::radio('is_planned', 1, TRUE) !!} Yes
                                {!! Form::radio('is_planned', 0, TRUE) !!} No
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="exampleInputFile">Leave Reason</label>
                            {{Form::text('leave_reason', null, array('class'=>'form-control'))}}
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::hidden('id', null)}}
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('.select2').select2();
        //fetchDesignation();
        $(document).on("change","#department",{target : $("#designation")},fetchDesignation);
        function fetchDesignation(event)
        {
            var depttId = $(this).val();
            var designationInput = event.data.target;
            var URL = window.location.origin+'/';
            var options = '<option></option>';
            $.ajax({
                url : URL+"get/designation/"+depttId,
                type: 'get',
                success : function(response){
                    var designation = response;
                    $.each(designation,function(id,val){
                        options += "<option value='"+id+"'>"+val+"</option>";
                    })
                },
                complete: function(){
                    designationInput.html(options);
                    designationInput.select2();
                }
            });
        }

       // $('.datepicker').datepicker({format:'dd-mm-yyyy',  startDate: new Date()});
        $('.datepicker_new').datepicker({format:'dd-mm-yyyy', autoclose: true});
    </script>
@endsection
