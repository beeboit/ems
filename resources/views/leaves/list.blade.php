@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <section class="content-header">
        <h1>
            Employee
            <small> Leave List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Employee Leave List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                @if(Session::has('flash_message'))
                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Leave List</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="row">
                            {{Form::model($details, array('method'=>'get'))}}
                                <div class="form-group col-md-3">
                                    <label for="exampleInputEmail1">Select Employee</label>
                                    {{Form::select('fk_employeeId', $employeeList, null, array('class'=>'form-control select2', 'data-placeholder'=>'Select Employee'))}}
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputEmail1">Leave Types</label>
                                    {{Form::select('leave_type', leave_types(), null, array('class'=>'form-control select2', 'data-placeholder'=>'Select Leave Type'))}}
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Leave Date:</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {{Form::text('leave_date', null, array('class'=>'form-control pull-right datepicker', 'autocomplete'=>'off'))}}
                                    </div>
                                </div>
                                <div class="form-group col-md-3 " style="padding-top: 22px;">
                                    <input type="submit" class="btn btn-primary" value="Search">
                                    <a href="{{url(route('leave.list'))}}" class="btn btn-success"><i class="fa fa-refresh"></i> Clear Filters</a>
                                </div>
                            {{Form::close()}}
                        </div>
                        <table id="data" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Leave Date</th>
                                <th>Employee</th>
                                <th>Leave Type</th>
                                <th>Planned</th>                                
                                <th>Reason</th>
                                <th>Edit Details</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($leaveList as $leave)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($leave['leave_date'])->format('d-M-Y')}}</td>
                                    <td>{{$leave->employee->first_name .' '.$leave->employee->last_name}}<br>
                                         @if(isset($leave->employee->department->department)) <strong> Department : </strong> {{$leave->employee->department->department}} @endif <br>
                                         @if(isset($leave->employee->designation->designation)) <strong> Designation :</strong> {{$leave->employee->designation->designation}} @endif
                                    </td>
                                    <td>{{leave_types($leave['leave_type'])}} - {{paid_types($leave['is_paid'])}}</td>
                                    <td>@if($leave['is_planned'] == 1) YES @else NO @endif</td>
                                    <td>{{$leave['leave_reason']}}</td>
                                    <td><a href="{{route('leave.show', ['id'=>$leave['id']])}}" class="btn btn-primary"> Edit Details</a></td>
                                    <td>
                                        {{Form::open(array('url'=>route('leave.destroy', ['leave'=>$leave]), 'method'=>'delete'))}}
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        {{Form::close()}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        <div class="pagination pull-right">{{$leaveList->links()}}</div>
                    </div>
                    <!-- /.box-body -->


                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('.select2').select2();
        $(function () {
            $('#data').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false
            })
        })
        $('.datepicker').datepicker({format:'dd-mm-yyyy', autoclose: true});
    </script>
@endsection
