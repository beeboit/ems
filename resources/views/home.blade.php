@extends('layouts.master')
@section('content')
    <style>
        .fc-time{
            display: none !important;
        }
        .green{
            background: green;
        }
    </style>
    <link rel="stylesheet" href="{{url('bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{url('bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
    <section class="content-header">
        <h1>
            Leave
            <small> Details</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Leave Details</li>
        </ol>
    </section>
    <section class="content">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{url('bower_components/moment/moment.js')}}"></script>
    <script src="{{url('bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
    <script>
        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        var d    = date.getDate(),
                m    = date.getMonth(),
                y    = date.getFullYear()
        $('#calendar').fullCalendar({
            header    : {
                //left  : 'prev,next today',
                left: '',
                center: 'title',
                right: 'prev,next today'
                //right : 'month,agendaWeek,agendaDay'
            },

            //Random default events
            events    : [
                    @php $leave_array = []; @endphp
                    @foreach($leaves as $leave)
                        {
                            title          : '{{$leave->employee->first_name}} -  leave_types($leave['leave_type']) }}',
                            start          : new Date('{{\Carbon\Carbon::parse($leave['leave_date'])->format('Y, m, d')}}'),
                            backgroundColor: '#DD4B39', //red
                            borderColor    : '#DD4B39' //red
                        },
                        @php $leave_array[] = \Carbon\Carbon::parse($leave['leave_date'])->format('Y-m-d'); @endphp
                   @endforeach

                    @foreach($public_holidays as $public_holiday) 
                       @php  $leave_array[] = \Carbon\Carbon::parse($public_holiday['date'])->format('Y-m-d'); @endphp
                        @php $date = \Carbon\Carbon::parse($public_holiday['date'])->format('Y, m, d'); @endphp
                        {
                            title          : "{{ $public_holiday['holiday'] }}",
                            start          : new Date('{{$date}}'),
                            backgroundColor: 'green', //red
                            borderColor    : 'green', //red
                        },
                   @endforeach
                    @if(isset($start_day))
                        @php 
                            $begin = new DateTime($start_day );
                            $end   = new DateTime( $today );
                        @endphp

                        @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                            @php $formattedDate = $i->format("Y-m-d"); @endphp
                            @if(!in_array($formattedDate, $leave_array))
                                {
                                    title          : "Present",
                                    start          : new Date('{{$formattedDate}}'),
                                    backgroundColor: 'green', //red
                                    borderColor    : 'green', //red
                                },
                            @endif
                        @endfor
                   @endif
                   
                  
            ],
            editable  : false,
            droppable : false, // this allows things to be dropped onto the calendar !!!

        })

        @if(isset($start_day))
            @php 
                  $begin = new DateTime($start_day );
                  $end   = new DateTime( $today );
            @endphp
             @for($i = $begin; $i <= $end; $i->modify('+1 day'))
                $('.fc-day[data-date={{$i->format("Y-m-d")}}]').css('background', 'green');
            @endfor
        @endif
        @foreach($public_holidays as $public_holiday) 
            @php $date = \Carbon\Carbon::parse($public_holiday['date'])->format('Y-m-d'); @endphp
            $('.fc-day[data-date={{$date}}]').css('background', 'green');
        @endforeach
        @foreach($leaves as $leave) 
            @php $date = \Carbon\Carbon::parse($leave['leave_date'])->format('Y-m-d'); @endphp
            $('.fc-day[data-date={{$date}}]').css('background', '#DD4B39');
        @endforeach
    </script>
   
@endsection