@extends('layouts.master')
@section('content')
    <style>
        .fc-time{
            display: none !important;
        }
         .red, .fc-content{
            font-size: 10px !important;
            color:#fff !important;
        }
       
    </style>
    <link rel="stylesheet" href="{{url('bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{url('bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
    <section class="content-header">
        <h1>
            Leave
            <small> Dashboard</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Leave Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-4">
                @if(count($public_holidays) > 0)
                    <div class="col-md-12 no-padding">
                         <div class="box box-primary">
                             <div class="box-header with-border">
                                <h3 class="box-title">Upcoming Holidays</h3>
                                <div class="box-tools">
                                    <img src="{{url('dist/img/air.png')}}" height="25">
                                </div>
                            </div>
                            
                            <!-- /.box-body -->
                            <div class="box-body">
                                <ul>
                                    @foreach($public_holidays as $public_holiday)
                                        <li><strong>{{$public_holiday['holiday']}}</strong> : {{\Carbon\Carbon::parse($public_holiday['date'])->format('d F, Y')}}</li>
                                    @endforeach
                                </ul>
                             </div>
                        </div>
                    </div>
                @endif
                @if(count($upcoming_birthdays) > 0)
                    <div class="col-md-12 no-padding">
                         <div class="box box-primary">
                             <div class="box-header with-border">
                                <h3 class="box-title">Upcoming Birthdays</h3>
                                <div class="box-tools">
                                    <img src="{{url('dist/img/cake.png')}}" height="25">
                                </div>
                            </div>
                           
                            <!-- /.box-body -->
                            <div class="box-body">
                                <ul>
                                    @foreach($upcoming_birthdays as $upcoming_birthday)
                                        <li><strong>{{$upcoming_birthday['first_name']. ' '. $upcoming_birthday['last_name']}}</strong> : {{\Carbon\Carbon::parse($upcoming_birthday['date_of_birth'])->format('d F')}}</li>
                                    @endforeach
                                </ul>
                             </div>
                        </div>
                    </div>
                @endif
                @if(count($employeesOnLeaveToday) > 0)
                    <div class="col-md-12 no-padding">
                         <div class="box box-primary">
                             <div class="box-header with-border">
                                <h3 class="box-title">Employees On Leave Today</h3>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-body">
                                <ul>
                                    @foreach($employeesOnLeaveToday as $leave)
                                        <li @if(hr_permission()) title="{!! $leave['leave_reason'] !!}" @endif><strong>{{$leave->employee->first_name. ' '. $leave->employee->last_name }}</strong> : {{\Carbon\Carbon::parse($leave['date'])->format('d F, Y')}} ( {{leave_types($leave['leave_type']) }})</li>
                                    @endforeach
                                </ul>
                             </div>
                        </div>
                    </div>
                @endif

                @if(count($awards) > 0)
                    @foreach($awards as $award)
                        @if(isset($award->employee->picture) and $award->employee->picture != null)
                               @php $imgUrl = url('employee_pictures/'.$award->employee->picture); @endphp
                            @else
                             @php $imgUrl = url('dist/img/default_avatar.png'); @endphp
                        @endif
                        <div class="col-md-12 no-padding">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Employee of the Month({{\Carbon\Carbon::parse($award['month_year'])->format('F')}})</h3>
                                     <div class="box-tools">
                                    <img src="{{url('dist/img/trophy.png')}}" height="25">
                                </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-body">
                                    <div class="box-body box-profile">
                                      <img class="profile-user-img img-responsive img-circle" src="{{ $imgUrl}}" alt="User profile picture">

                                      <h3 class="profile-username text-center">{{$award->employee->first_name}}</h3>

                                      <p class="text-muted text-center">In Recognition of : {!! $award['award_in_recognition_of'] !!} throughout the month of {{\Carbon\Carbon::parse($award['month_year'])->format(' F, Y')}} </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

               
               
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{url('bower_components/moment/moment.js')}}"></script>
    <script src="{{url('bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
    <script>
        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        var d    = date.getDate(),
                m    = date.getMonth(),
                y    = date.getFullYear()
        $('#calendar').fullCalendar({
            header    : {
                //left  : 'prev,next today',
                left: '',
                center: 'title',
                right: '',
                 //right: 'prev,next today'
                //right : 'month,agendaWeek,agendaDay'
            },

            //Random default events
            events    : [
                    @php $leave_array = []; @endphp
                    @foreach($leaves as $leave)
                        {
                            title          : '{{$leave->employee->first_name}} -  {{leave_types($leave['leave_type']) }}',
                            start          : new Date('{{\Carbon\Carbon::parse($leave['leave_date'])->format('Y, m, d')}}'),
                            backgroundColor: '#DD4B39', //red
                            borderColor    : '#DD4B39' //red
                        },
                        @php $leave_array[] = \Carbon\Carbon::parse($leave['leave_date'])->format('Y-m-d'); @endphp
                   @endforeach

                    @foreach($public_holidays as $public_holiday) 
                       @php  $leave_array[] = \Carbon\Carbon::parse($public_holiday['date'])->format('Y-m-d'); @endphp
                        @php $date = \Carbon\Carbon::parse($public_holiday['date'])->format('Y, m, d'); @endphp
                        {
                            title          : "{{ $public_holiday['holiday'] }}",
                            start          : new Date('{{$date}}'),
                            backgroundColor: '#f39c12', //red
                            borderColor    : '#f39c12', //red
                        },
                   @endforeach
                          
                  
            ],
            editable  : false,
            droppable : false, // this allows things to be dropped onto the calendar !!!

        })

        
        @foreach($public_holidays as $public_holiday) 
            @php $date = \Carbon\Carbon::parse($public_holiday['date'])->format('Y-m-d'); @endphp
            $('.fc-day[data-date={{$date}}]').addClass('orange');
        @endforeach
        @foreach($leaves as $leave) 
            @php $date = \Carbon\Carbon::parse($leave['leave_date'])->format('Y-m-d'); @endphp
            $('.fc-day[data-date={{$date}}]').css('background', '#DD4B39');
        @endforeach
    </script>
   
@endsection