@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Award List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Award List</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body"> <h3 class="text-center">Employee of the month</h3>
                           
                        <table id="data" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Appreciation For The Month Of</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Award In recognition of</th>
                                <th>Certificate</th>
                                <th>View/Edit Details</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($awardList as $award)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{\Carbon\Carbon::parse($award['month_year'])->format(' F, Y')}}</td>
                                    <td>{{$award->employee->first_name}}</td>
                                    <td>@if(isset($award->employee->department->name)) {{$award->employee->department->name}}@endif</td>
                                    <td>{{$award['award_in_recognition_of']}}</td>
                                    <td>
                                        
                                         @if(isset($award['certificate'])) 
                                  <a  href="{{url('employee_documents/'.$award['fk_employeeId'].'/'.$award['certificate'])}}" target="_blank">
                                        <img src="{{url('dist/img/pdf.png')}}" height=50><br>
                                           <span class="btn-xs btn-primary">View Certificate</span>
                                    </a><br>
                                @endif
                                    </td>
                                    <td><a href="{{route('award.show', ['id'=>$award['id']])}}" class="btn btn-primary"> Edit</a></td>
                                    <td>
                                        {{Form::open(array('url'=>route('award.destroy', ['award'=>$award]), 'method'=>'delete'))}}
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        {{Form::close()}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->


                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#data').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
