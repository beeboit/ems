@extends('layouts.master')
@section('content')
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <style>
        .datepicker > div {
    display: block ;
}
    </style>
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Awards Details</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Awards Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {{Form::model($details, array('url'=>route('award.store' ), 'files'=> true))}}
                    <div class="box-body">
                        <div class="form-group col-md-3">
                            <label for="exampleInputEmail1">Award In Recognition Of</label>
                            {{Form::text('award_in_recognition_of',  null, array('class'=>'form-control'))}}
                        </div>
                        
                        <div class="form-group col-md-3">
                            <label for="exampleInputFile">Employee</label>
                            {{Form::select('fk_employeeId', get_all_employees(), null, array('class'=>'form-control select2', 'id'=>'department', 'data-placeholder'=>'Select Employee'))}}
                        </div>

                        
                        @if($details != null)
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Month</label>
                                {{Form::text('month_year',  \Carbon\Carbon::parse($details['month_year'])->format('Y-m'), array('class'=>'form-control form-control-1 input-sm from monthYearPicker', 'autocomplete'=>'off'))}}
                            </div>
                        @else
                            <div class="form-group col-md-3">
                                <label for="exampleInputEmail1">Month</label>
                                {{Form::text('month_year',  null, array('class'=>'form-control form-control-1 input-sm from monthYearPicker', 'autocomplete'=>'off'))}}
                            </div>
                        @endif
                        <div class="form-group col-md-3">
                            <label for="exampleInputFile">Certificate</label>
                            @if($details != null and isset($details['certificate'])) 
                                  <a  href="{{url('employee_documents/'.$details['fk_employeeId'].'/'.$details['certificate'])}}" target="_blank">
                                        <img src="{{url('dist/img/pdf.png')}}"><br>
                                           <span class="btn-xs btn-primary">View Certificate</span>
                                    </a><br>
                                @endif
                                <input type="file" name="certificate" accept="application/pdf">
                           
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::hidden('id', null)}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
     <script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        var startDate = new Date();
        $('.select2').select2();
        $('.monthYearPicker').datepicker({
            autoclose: true,
            minViewMode: 1,
            format: 'yyyy-mm'
        }).on('changeDate', function(selected){
        FromEndDate = new Date(selected.date.valueOf());
        FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
        //$('.monthYearPicker').datepicker('setEndDate', FromEndDate);
      }); 
    </script>
@endsection
