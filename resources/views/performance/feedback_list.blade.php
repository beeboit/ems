@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <section class="content-header">
        <h1>
            Feedback
            <small> List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Feedback List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Feedback List</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table id="data" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Reviewing Month</th>
                                <th>Name</th>
                                <th>Reviewing Supervisor</th>
                                <th>Department</th>
                                <th>Designation</th>
                                <th>Status</th>
                                <th>View Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($feedbackList as $feedback)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{\Carbon\Carbon::parse($feedback['reviewing_month'])->format(' M, Y')}}</td>
                                    <td>{{$feedback->employee->first_name}}</td>
                                    <td>{{$feedback->reviewing_supervisor_details->first_name}}</td>
                                    <td>{{$feedback->employee->department->department}}</td>
                                    <td>{{$feedback->employee->designation->designation}}</td>
                                    <td>
                                        @if($feedback['my_accomplishments'] != null and $feedback['employee_strongest_point'] != null)
                                            <small class="label label-success"><i class="fa fa-check"></i> Complete</small>
                                        @else
                                            <small class="label label-danger"><i class="fa fa-close"></i> Incomplete</small>
                                        @endif
                                    </td>
                                    <td>
                                       <a href="{{route('employee_feedback', ['id'=>$feedback['id']])}}" class="btn-sm btn-primary">View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-right"> {{$feedbackList->links()}} </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#data').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
