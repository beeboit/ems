<style>
    .fa-star-o{
        color:#6b5959;

    }
    .star{
        color: orange;
    }

</style>

<div class="box-body col-md-offset-1">
    <h3 class="text-center">JOB PERFORMANCE</h3>
      <!-- LINE CHART -->
         
    <table id="data" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Sr. No</th>
            <th>Evaluation Item</th>
            <th>Service Rating</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1.</td>
            <td>Quality of Work</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['work_quality'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Dependability</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['dependability'])
                        <i class="fa fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Job Knowledge</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['job_knowledge'])
                        <i class="fa fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Communication Skills</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['communication_skills'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Personality</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['personality'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Management Ability</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['management_ability'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>7.</td>
            <td>Contribution to group</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['group_contribution'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>8.</td>
            <td>Productivity</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['productivity'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>9.</td>
            <td>Achievement of Goals</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['goal_achievement'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        <tr>
            <td>10.</td>
            <td>Punctuality</td>
            <td>
                @for($i=1; $i<=5;$i++)
                    @if($i <= $details['punctuality'])
                        <i class="fa  fa-star star"></i>
                    @else
                        <i class="fa  fa-star-o"></i>
                    @endif
                @endfor
            </td>
        </tr>
        </tbody>
    </table>
    <h3 class="text-center">PERFORMANCE SUMMARY</h3>
    <div class="form-group" style="min-height:100px;">
        <label for="exampleInputEmail1">1. What are employee's strongest points?</label>
        <p>{!! nl2br($details['employee_strongest_point']) !!}</p>
    </div>
    <div class="form-group" style="min-height:100px;">
        <label for="exampleInputEmail1">2. What are employee's weakest points?</label>
        <p>{!! nl2br($details['employee_weakest_point']) !!}</p>
    </div>
    <div class="form-group" style="min-height:100px;">
        <label for="exampleInputFile">3. What can the employee do to be more effective or make improvements?</label>
        <p>{!! nl2br($details['employee_improvement_needed']) !!}</p>
    </div>
    <div class="form-group" style="min-height:100px;">
        <label for="exampleInputFile">4. What additional trainings would benefit the employees?</label>
        <p>{!! nl2br($details['additional_beneficial_training']) !!}</p>
    </div>

</div>
<!-- /.box-body -->
