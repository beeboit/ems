<div class=" col-md-offset-1">
    <h3 class="box-title text-center">Employee Feedback</h3>
    <!-- /.box-header -->
    <!-- form start -->
    {{Form::model($details, array('url'=>route('self_feedback')))}}
        <div class="box-body">
            <div class="form-group">
                <label for="exampleInputEmail1">1. What are your most important accomplisments over the last month?</label>
                {{Form::textarea('my_accomplishments', null, array('class'=>'form-control', 'required', 'rows'=>3))}}
                @error('my_accomplishments')
                    <span class="text-danger" role="alert">
                         <strong>This field is required.</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">2. What are your weakest area and how could you improve?</label>
                {{Form::textarea('my_weakest_area', null, array('class'=>'form-control', 'required', 'rows'=>3))}}
                @error('my_weakest_area')
                    <span class="text-danger" role="alert">
                         <strong>This field is required.</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputFile">3. What are the other concerns you would like to discuss with management?</label>
                {{Form::textarea('concerns_with_management', null, array('class'=>'form-control', 'required', 'rows'=>3))}}
                @error('concerns_with_management')
                    <span class="text-danger" role="alert">
                         <strong>This field is required.</strong>
                    </span>
                @enderror
            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer text-right">
            {{Form::hidden('id', null)}}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>