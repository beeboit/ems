            <!-- form start -->
            {{Form::model($details, array('url'=>route('evaluate_performance')))}}
              <div class="box-body col-md-offset-1">
				  <h3 class="text-center">JOB PERFORMANCE</h3>
				  <table id="data" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sr. No</th>
                            <th>Evaluation Item</th>
                            <th>Service Rating</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
							<td>1.</td>
							<td>Quality of Work</td>
							<td>
                                {{Form::select('work_quality', $ratings, null, array('class'=>'form-control select2','data-placeholder'=>'Service Rating'))}}
                                @error('work_quality') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>2.</td>
							<td>Dependability</td>
							<td>{{Form::select('dependability', $ratings, null, array('class'=>'form-control select2',  'data-placeholder'=>'Service Rating'))}}
                                @error('dependability') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>3.</td>
							<td>Job Knowledge</td>
							<td>{{Form::select('job_knowledge', $ratings, null, array('class'=>'form-control select2',  'data-placeholder'=>'Service Rating'))}}
                                @error('job_knowledge') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>4.</td>
							<td>Communication Skills</td>
							<td>{{Form::select('communication_skills', $ratings, null, array('class'=>'form-control select2', 'data-placeholder'=>'Service Rating'))}}
                                @error('communication_skills') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>5.</td>
							<td>Personality</td>
							<td>{{Form::select('personality', $ratings, null, array('class'=>'form-control select2', 'data-placeholder'=>'Service Rating'))}}
                                @error('personality') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>6.</td>
							<td>Management Ability</td>
							<td>{{Form::select('management_ability', $ratings, null, array('class'=>'form-control select2', 'data-placeholder'=>'Service Rating'))}}
                                @error('management_ability') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>7.</td>
							<td>Contribution to group</td>
							<td>{{Form::select('group_contribution', $ratings, null, array('class'=>'form-control select2',  'data-placeholder'=>'Service Rating'))}}
                                @error('group_contribution') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>8.</td>
							<td>Productivity</td>
							<td>{{Form::select('productivity', $ratings, null, array('class'=>'form-control select2',  'data-placeholder'=>'Service Rating'))}}
                                @error('productivity') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>9.</td>
							<td>Achievement of Goals</td>
							<td>{{Form::select('goal_achievement', $ratings, null, array('class'=>'form-control select2', 'data-placeholder'=>'Service Rating'))}}
                                @error('goal_achievement') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
                        <tr>
							<td>10.</td>
							<td>Punctuality</td>
							<td>{{Form::select('punctuality', $ratings, null, array('class'=>'form-control select2', 'data-placeholder'=>'Service Rating'))}}
                                @error('punctuality') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                            </td>
						</tr>
					</tbody>
                </table>
				 <h3 class="text-center">PERFORMANCE SUMMARY</h3>
                <div class="form-group">
                  <label for="exampleInputEmail1">1. What are employee's strongest points?</label>
                  {{Form::textarea('employee_strongest_point', null, array('class'=>'form-control', 'required', 'rows'=>3))}}
                    @error('employee_strongest_point') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">2. What are employee's weakest points?</label>
                  {{Form::textarea('employee_weakest_point', null, array('class'=>'form-control', 'required', 'rows'=>3))}}
                    @error('employee_weakest_point') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">3. What can the employee do to be more effective or make improvements?</label>
                  {{Form::textarea('employee_improvement_needed', null, array('class'=>'form-control', 'required', 'rows'=>3))}}
                    @error('employee_improvement_needed') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">4. What additional trainings would benefit the employees?</label>
                  {{Form::textarea('additional_beneficial_training', null, array('class'=>'form-control', 'required', 'rows'=>3))}}
                    @error('additional_beneficial_training') <span class="text-danger" role="alert"> <strong>This field is required.</strong></span>@enderror
                </div>
                @if(hr_permission() and $details['employee_strongest_point'] != null)
                    <div class="form-group">
                          <label for="exampleInputFile">5. HR comments</label>
                          {{Form::textarea('hr_comments', null, array('class'=>'form-control',  'rows'=>3))}}
                    </div>
                @endif
                {{Form::hidden('id', null)}}
              </div>
              <!-- /.box-body -->

              <div class="box-footer text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>

