


                    <!-- /.box-header -->

                    <div class="box-body col-md-offset-1">
                        <h3 class="box-title text-center">Employee Feedback</h3>

                        <div class="form-group" style="min-height:100px;">
                            <label for="exampleInputEmail1">1. What are your most important accomplisments over the last month?</label>
                            <p>{!! nl2br($details['my_accomplishments']) !!}</p>
                        </div>
                        <div class="form-group" style="min-height:100px;">
                            <label for="exampleInputEmail1">2. What are your weakest area and how could you improve?</label>
                            <p>{!! nl2br($details['my_weakest_area']) !!}</p>
                        </div>
                        <div class="form-group" style="min-height:100px;">
                            <label for="exampleInputFile">3. What are the other concerns you would like to discuss with management?</label>
                            <p>{!! nl2br($details['concerns_with_management']) !!}</p>
                        </div>

                    </div>
                    <!-- /.box-body -->




