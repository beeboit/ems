@extends('layouts.master')
@section('content')
    <style>
        .emp_details{
            font-size: 18px;
        }
    </style>
    <section class="content-header">
        <h1>
            Employee
            <small> Feedback</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Employee Feedback</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif

            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->

                    <div class="box box-primary">

                        <ul class="nav nav-pills ">
                            <li class="active"><a href="#employee_feedback" data-toggle="tab">EMPLOYEE FEEDBACK</a></li>
                            @if(hr_permission() || manager_permission($details['fk_employeeId']) || $details['reviewing_supervisor']== authId())
                                <li><a href="#job_performance" data-toggle="tab">JOB PERFORMANCE</a></li>
                            @endif
                        </ul>
                        <div class="box-header with-border emp_details col-md-offset-1">
                            <h3 class="text-center"><strong>EMPLOYEE PERFORMANCE EVALUATION</strong></h3>

                            <div class="row ">
                                <div class="col-md-3">Employee Name:</div>
                                <div class="col-md-9">{{$details->employee->first_name}} {{$details->employee->last_name}}</div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3">Job Title: </div>
                                <div class="col-md-3">{{$details->employee->designation->designation}}</div>
                                <div class="col-md-3">Department:</div>
                                <div class="col-md-3">{{$details->employee->department->department}}</div>
                            </div>
                            <div class="row ">
                                <div class="col-md-3">Reviewing Supervisor: </div>
                                <div class="col-md-3">{{$details->reviewing_supervisor_details->first_name}} {{$details->reviewing_supervisor_details->last_name}}</div>
                                <div class="col-md-3">Review Month:</div>
                                <div class="col-md-3">{{\Carbon\Carbon::parse($details['reviewing_month'])->format('F, Y')}}</div>
                            </div>
                        </div>
                        @if($details['work_quality'] != null and $details['work_quality'] > 0)

                        <div class="row">
                             <div class="col-md-8 col-md-offset-2 chart-responsive">
                                  <canvas id="barChart" width="400" height="400" ></canvas>
                            </div>
                        </div>
                        @endif
                       
                        <div class="tab-content">
                            <div id="employee_feedback" class="tab-pane fade in active">
                                @if(hr_permission() || manager_permission($details['fk_employeeId']) || $details['fk_employeeId']== authId()|| $details['reviewing_supervisor']== authId())
                                    @if($details['my_accomplishments'] == null and $details['fk_employeeId']== authId())
                                        @include('performance.fragments.employee_feedback')
                                    @else
                                        @include('performance.fragments.employee_feedback_details')
                                    @endif
                                @endif
                            </div>

                            <div id="job_performance" class="tab-pane fade ">
                                @if(hr_permission() || $details['reviewing_supervisor']== authId() || (manager_permission($details['fk_employeeId']) and \Auth::user()->department->manager != authId()))
                                
                                    @if(($details['employee_strongest_point'] == null and $details['reviewing_supervisor']== authId()) || (hr_permission() and $details['fk_employeeId'] != authId() ))
                                        @include('performance.fragments.employee_evaluation_form')
                                    @else
                                        @if($details['fk_employeeId'] != authId())
                                            @include('performance.fragments.employee_evaluation_details')
                                        @endif
                                    @endif
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </section>
@endsection

@if($details['work_quality'] != null and $details['work_quality'] > 0)
@section('chart_scripts')
    <script src="{{url('bower_components/chart.js/Chart.js')}}"></script>
    <script>
        $(function () {
            var areaChartData = {
      labels  :  ['Quality Of work', 'Dependability', 'Job Knowledge', 'Communication Skills', 'Personality', 'Management Ability', 'Contribution to group', 'Productivity', 'Achievement of Goals', 'Punctuality'],
      datasets: [
        {
          label               : 'EMPLOYEE PERFORMANCE',
           fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [{{$details['work_quality']}},{{$details['dependability']}},{{$details['job_knowledge']}},{{$details['communication_skills']}},{{$details['personality']}},{{$details['management_ability']}},{{$details['group_contribution']}},{{$details['productivity']}},{{$details['goal_achievement']}},{{$details['punctuality']}}],
        }
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 5,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

        var lineChartCanvas          = $('#barChart').get(0).getContext('2d')
        var lineChart                = new Chart(lineChartCanvas)
        var lineChartOptions         = areaChartOptions
        lineChartOptions.datasetFill = false
        lineChart.Line(areaChartData, lineChartOptions)
   


   /* var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    barChartData.datasets[0].fillColor   = '#00a65a'
    barChartData.datasets[0].strokeColor = '#00a65a'
    barChartData.datasets[0].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 1,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 1,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : false
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)*/
     });
</script>

@endsection
@endif



