@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Designation List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Designation List</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table id="data" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Designation</th>
                                <th>Department</th>
                                <th>View Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($designationList as $designation)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$designation['designation']}}</td>
                                    <td>{{departments($designation['fk_departmentId'])}}</td>
                                    <td><a href="{{route('designation.show', ['id'=>$designation['id']])}}" class="btn btn-primary"> View Details</a></td>
                                  
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->


                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#data').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
