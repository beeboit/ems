<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EMS - Beebo Tech</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    
    <link rel="stylesheet" href="{{url('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
     <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('bower_components/font-awesome/css/font-awesome.min.css')}}">
      <link rel="stylesheet" href="{{url('bower_components/Ionicons/css/ionicons.min.css')}}">
   <!-- Ionicons -->
    <link rel="stylesheet" href="{{url('bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('dist/css/AdminLTE.min.css')}}">
      <link href="{{url('css/datepicker.min.css')}}" rel="stylesheet">
   <link rel="stylesheet" href="{{url('dist/css/style.css?v=1235')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{url('dist/css/skins/_all-skins.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
    .pmd-floating-action span{
    position: absolute !important;
    top: -15px !important;
    right: 7px;
    text-align: center;
    font-size: 12px;
    padding: 5px 8px;
    line-height: .9;
    }
.pmd-floating-action {
    position: fixed;
    /*right: 1rem;*/
    bottom: 1rem;
    z-index: 1000;
}
.btn.pmd-btn-fab {
    width: 56px;
    min-width: 56px;
    height: 56px;
    padding: 12px 0;
    border-radius: 50%;
}
.btn.pmd-btn-raised {
    box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
}
.floating-btn-custon {
    background: #f39c12 none repeat scroll 0 0;
    color: #fff;
    z-index: 1000;
}
.pmd-floating-action-btn {
    position: relative;
    display: block;
    transition: all .2s ease-in-out;
    transition: all .3s cubic-bezier(.55,0,.1,1);
}
.project-tab .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
    color: #222d32 !important;
    background-color: transparent;
    border-color: transparent transparent #222d32 !important;
    border-bottom: 3px solid !important;
    font-size: 16px;
    font-weight: 600;
}
.project-tab .nav-tabs a{
    text-decoration: none;
    color: #fff;
    font-weight: 600;
    padding: 8px 25px;
}
.project-tab .nav-tabs a:hover{
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 3px solid !important;
}
.project-tab nav{
    text-align: center;
}
.project-tab .nav-tabs{
    border-bottom: 0;
    margin-bottom: 20px;
}

.fa-star-o{
        color:#6b5959;

    }
    .star{
        color: orange;
    }
</style>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{url('/')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>E</b>MS</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Employee</b>Management</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Notifications: style can be found in dropdown.less -->
                    <?php $notifications = get_notifications(); ?>
                    @if(count($notifications) > 0)
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o"></i>
                                <span class="label label-warning">{{count($notifications)}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have {{count($notifications)}} notifications</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        @foreach($notifications as $notification)
                                            <li>
                                                <a href=@if(isset($notification['url'])) {{$notification['url']}} @endif>
                                                    <i class="fa fa-users text-aqua"></i> {{$notification['notification']}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <!-- Tasks: style can be found in dropdown.less -->
                    @if(isset(Auth::user()->picture) and Auth::user()->picture != null)
                               @php $profilePic = url('employee_pictures/'.Auth::user()->picture); @endphp
                            @else
                             @php $profilePic = url('dist/img/default_avatar.png'); @endphp
                        @endif
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ $profilePic }}" height=25 alt="User Image">
                            <span class="hidden-xs"> {{ Auth::user()->first_name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ $profilePic }}" style="height:auto; max-height:80px;" class="" alt="User Image">

                                <p>
                                    {{ Auth::user()->first_name }} - {{ Auth::user()->designation->designation }}
                                    <!--<small>Member since Nov. 2012</small>-->
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                               <div class="pull-left">
                                    <a href="{{url('change/password')}}" class="btn btn-default btn-flat">Change Password</a>
                                </div>

                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar project-tab">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ $profilePic }}" height=25 class="" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->first_name }} </p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->

            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    @if(hr_permission())
                        @php $manager="active in"; $self=""; @endphp
                        @if(in_array(Request::url(), [url('/'), url('my/profile'), url('attendance'), url('team/performance/evaluation/list'), url('my/performance/evaluation/list')]))
                            @php $self="active in"; $manager=""; @endphp
                        @endif
                        <a class="nav-item nav-link {{$manager}}" id="manager-tab" data-toggle="tab" href="#manager" role="tab" aria-controls="manager" aria-selected="false">Manager</a>  
                        <a class="nav-item nav-link {{ $self }}" id="self-tab" data-toggle="tab" href="#self" role="tab" aria-controls="self" aria-selected="true">Self</a> 
                    @endif 
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                
                @if(hr_permission())
                    <div class="tab-pane fade {{$manager}}" id="manager" role="tabpanel" aria-labelledby="manager-tab">
                        <ul class="sidebar-menu" data-widget="tree">
                             <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-trophy"></i> <span>Awards and Appreciations</span>
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{route('award.new')}}"><i class="fa fa-plus-square"></i> Add</a></li>
                                        <li><a href="{{url('award')}}"><i class="fa fa-list"></i> List</a></li>
                                    </ul>
                                </li>
                               <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-users"></i> <span>Department</span>
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{route('department.new')}}"><i class="fa fa-plus-square"></i> Add</a></li>
                                        <li><a href="{{url('department')}}"><i class="fa fa-list"></i> List</a></li>
                                    </ul>
                                </li>

                                 <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-suitcase"></i> <span>Designation</span>
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{route('designation.new')}}"><i class="fa fa-plus-square"></i> Add</a></li>
                                        <li><a href="{{url('designation')}}"><i class="fa fa-list"></i> List</a></li>
                                    </ul>
                                </li>

                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-users"></i> <span>Employee</span>
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{route('employee.new')}}"><i class="fa fa-user-plus"></i> Add</a></li>
                                        <li><a href="{{url('employee')}}"><i class="fa fa-list"></i> List</a></li>
                                    </ul>
                                </li>
                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-calendar-o"></i> <span>Leave</span>
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{route('leave.new')}}"><i class="fa fa-plus-square"></i> Add</a></li>
                                        <li><a href="{{url('leave')}}"><i class="fa fa-list"></i> List</a></li>
                                    </ul>
                                </li>

                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-calendar-o"></i> <span>Public Holidays</span>
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{route('publicholiday.new')}}"><i class="fa fa-plus-square"></i> Add</a></li>
                                        <li><a href="{{url('publicholiday')}}"><i class="fa fa-list"></i> List</a></li>
                                    </ul>
                                </li>

                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-suitcase"></i> <span>Project</span>
                                        <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{route('project.new')}}"><i class="fa fa-plus-square"></i> Add</a></li>
                                        <li><a href="{{url('project')}}"><i class="fa fa-list"></i> List</a></li>
                                    </ul>
                                </li>

                              
                               

                               
                                 <li><a href="{{route('employeePerformanceEvaluationList')}}"><i class="fa fa-list"></i> Employees Feedback</a></li>
                        </ul>
                    </div>
                @endif
                <div class="tab-pane fade  @if(!hr_permission()) active in @else {{ $self }} @endif" id="self" role="tabpanel" aria-labelledby="self-tab">
                    <ul class="sidebar-menu" data-widget="tree">
                         <li class="header">MAIN NAVIGATION</li>
                         <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                         <li><a href="{{url('my/profile')}}"><i class="fa fa-user"></i> <span>My Profile</span></a></li>
                         <li><a href="{{url('attendance')}}"><i class="fa fa-calendar-check-o"></i> <span>My Attendance</span></a></li>      
                        
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Employee Evaluation</span>
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                               @if(Auth::user()->id != 1) <li><a href="{{route('myPerformanceEvaluationList')}}"><i class="fa fa-list"></i>  Feedback Summary</a></li>@endif
                                <li><a href="{{route('myTeamPerformanceEvaluationList')}}"><i class="fa fa-list"></i> My Team's Feedback</a></li>
                               
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
            @yield('content')
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
   

    <!-- Control Sidebar -->

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
@if(count($notifications) > 0)
    <div class="menu pmd-floating-action" role="navigation">
        <a class="js-gitter-toggle-chat-button floating-btn-custon pmd-floating-action-btn btn pmd-btn-fab pmd-btn-raised btn-secondary " href="javascript:void(0);">            
            <i class="fa fa-bell fa-2x"></i>
            <span class="label label-danger">{{count($notifications)}}</span>
        </a>
    </div>
@endif
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('bower_components/fastclick/lib/fastclick.js')}}"></script>
@yield('scripts')
<!-- AdminLTE App -->
<script src="{{url('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('dist/js/demo.js')}}"></script>
<script>    
    $('.nav-tabs a').click(function (e) {
  $('.nav-tabs a').toggleClass('active')
})
</script>

@yield('chart_scripts')
</body>
</html>
