@extends('layouts.master')
@section('content')
    <link href="{{url('bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Project Details</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <!-- general form elements -->
                <div class="box box-primary">
                    
                    <!-- /.box-header -->
                    <!-- form start -->

                    {{Form::model($details, array('url'=>route('project.store')))}}
                    <div class="box-body">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Project Name</label>
                            {{Form::text('project_name',  null, array('class'=>'form-control'))}}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputFile">Project Start Date</label> 
                            {{Form::text('start_date', null, array('class'=>'form-control datepicker', 'autocomplete'=>'off'))}}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputFile">Project End Date</label> 
                            {{Form::text('end_date', null, array('class'=>'form-control datepicker', 'autocomplete'=>'off'))}}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputFile">Project Deadline </label> 
                            {{Form::text('deadline', null, array('class'=>'form-control datepicker', 'autocomplete'=>'off'))}}
                        </div>
                       
                        <div class="form-group col-md-12">
                            <label for="exampleInputFile">Employees involved in project</label>
                            {{Form::select('fk_employeeId[]', $allEmployees, $assignedToEmployees, array('class'=>'form-control select2', 'multiple'=>'multiple', 'id'=>'employee', 'data-placeholder'=>'Select Employees'))}}
                        </div>                    
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{Form::hidden('id', null)}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('.select2').select2();
        $('.datepicker').datepicker({format:'yyyy-mm-dd', autoclose: true});
    </script>
@endsection
