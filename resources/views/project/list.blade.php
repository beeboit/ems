@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <section class="content-header">
        <h1>
            &nbsp;
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Project List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Project List</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <table id="data" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Project</th>
                                <th>Start Date</th>
                                <th>Deadline Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th>View Details</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projectList as $project)
                                <tr @if($project['end_date'] != null and $project['end_date'] <= $today) style="background-color: green; color:#fff;" @endif>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$project['project_name']}}</td>
                                    <td>@if($project['start_date'] != null){{\Carbon\Carbon::parse($project['start_date'])->format('d F, Y')}} @endif</td>
                                    <td>@if($project['deadline_date'] != null) {{\Carbon\Carbon::parse($project['deadline_date'])->format('d F, Y')}} @endif</td>
                                    <td>@if($project['end_date'] != null) {{\Carbon\Carbon::parse($project['end_date'])->format('d F, Y')}} @endif</td>
                                    <td>
                                        @if($project['end_date'] != null and $project['end_date'] <= $today) 
                                            @for($i=1; $i<=5;$i++)
                                                <i class="fa fa-2x fa-star star"></i>
                                            @endfor
                                        @else 
                                            In Progress
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('project.show', ['id'=>$project['id']])}}" class="btn btn-primary"> Edit Details</a>
                                    </td>
                                    <td>
                                        {{Form::open(array('url'=>route('project.destroy', ['project'=>$project]), 'method'=>'delete'))}}
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        {{Form::close()}}
                                    
                                    </td>
                                  
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->


                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{url('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#data').DataTable({
                'paging'      : false,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
