@extends('layouts.master')
@section('content')
    <section class="content-header">
      <h1>
        My Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">My profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
                @if(isset($employeeDetails['picture']) and $employeeDetails['picture'] != null)
                    @php $imgUrl = url('employee_pictures/'.$employeeDetails['picture']); @endphp
                @else
                    @php $imgUrl = url('dist/img/default_avatar.png'); @endphp
                @endif
              <img class="profile-user-img img-responsive" src="{{$imgUrl}}" alt="User profile picture">

              <h3 class="profile-username text-center">{{$employeeDetails->first_name}} {{$employeeDetails->last_name}}</h3>

              <p class="text-muted text-center">@if(isset($employeeDetails->department->department)) {{$employeeDetails->department->department}} @endif</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Department</b> <a class="pull-right">@if(isset($employee->department->department)) {{$employee->department->department}} @endif</a>
                </li>
                <li class="list-group-item">
                  <b>Designation</b> <a class="pull-right">@if(isset($employee->designation->designation)) {{$employee->designation->designation}} @endif</a>
                </li>
                <li class="list-group-item">
                  <b>Email</b> <a class="pull-right">{{$employeeDetails->email}}</a>
                </li>
              </ul>

            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-envelope margin-r-5"></i> Personal Email</strong>

              <p class="text-muted">
               {{$employeeDetails->personal_email}}
              </p>

              <hr>

              <strong><i class="fa fa-phone margin-r-5"></i> Phone Number</strong>

              <p class="text-muted">{{$employeeDetails->phone_number}}</p>

              <hr>

              <strong><i class="fa fa-phone margin-r-5"></i>Altername Phone Number</strong>

              <p class="text-muted">{{$employeeDetails->alternate_phone_number}}</p>

              <hr>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-building margin-r-5"></i> Bank Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong> Bank Name</strong>

              <p class="text-muted">
               {{$employeeDetails->bank_name}}
              </p>

              <hr>

              <strong>IFSC</strong>

              <p class="text-muted">{{$employeeDetails->ifsc_code}}</p>

              <hr>

              <strong>Account Holder</strong>

              <p class="text-muted">{{$employeeDetails->account_holder}}</p>

              <hr>
               <strong>Account Number</strong>

              <p class="text-muted">{{$employeeDetails->account_number}}</p>

              <hr>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Details</a></li>
              <li><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <div class="row">
                  <div class="col-md-12">
                     <label for="inputEmail" class="col-sm-3 control-label"> Date of Joining</label>
                     <div class="col-sm-8">
                         {{ \Carbon\Carbon::parse($employeeDetails->date_of_joining)->format('d F, Y') }}
                     </div>
                  </div><hr>
                  <div class="col-md-12">
                      <label for="inputName" class="col-sm-3 control-label">Date of Birth</label>
                      <div class="col-sm-8">
                           {{ \Carbon\Carbon::parse($employeeDetails->date_of_birth)->format('d F, Y') }}
                      </div>
                  </div><hr>
                  <div class="col-md-12">
                      <label for="inputName" class="col-sm-3 control-label">Gender</label>
                      <div class="col-sm-8">
                          @if($employeeDetails->gender != null) {{ gender($employeeDetails->gender) }} @endif
                      </div>
                  </div><hr>

                  <div class="col-md-12">
                      <label for="inputName" class="col-sm-3 control-label">Official Phone Number</label>
                      <div class="col-sm-8">
                           {{ $employeeDetails->official_phone_number }}
                      </div>
                  </div><hr>
                </div>
                  
                <!-- /.post -->
              </div>
             
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                  {{Form::open(array('class'=>'form-horizontal', 'url'=>'change/password'))}}
                    @if(Session::has('flash_message'))
                        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                    @endif
                      <div class="form-group">
                          <label for="inputEmail" class="col-sm-3 control-label"> Password</label>
                          <div class="col-sm-8">
                              <input type="password" class="form-control" placeholder="Password" name="password">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="inputName" class="col-sm-3 control-label">Confirm Password</label>
                          <div class="col-sm-8">
                              <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                          </div>
                      </div>
                      <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">Change Password</button>
                      </div>
                  </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
@endsection